//
//  WishListViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 04/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol wishlistprotocol
- (void)buttonclick;


@end

@interface WishListViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UILabel *lblnowishlist;

@property (assign) id <wishlistprotocol> wishdelegate;
@end
