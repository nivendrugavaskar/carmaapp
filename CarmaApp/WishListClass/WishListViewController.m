//
//  WishListViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 04/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "WishListViewController.h"
#import "AppDelegate.h"
#import "WishListTableViewCell.h"
#import "ProductDescriptionViewController.h"
#import "FBSliderMenuViewController.h"

@interface WishListViewController ()<UITableViewDataSource,UITableViewDelegate,connectionprotocol,UIAlertViewDelegate>
{
    AppDelegate *app;
    Connection *conn;
    BOOL loadlist;
    
    NSString *selectedservice;
    NSMutableArray *wishlistarray;
    NSIndexPath *deletedindex;
}

@end

@implementation WishListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setbarbuttons];
    loadlist=FALSE;
    
#pragma set delegate
    self.wishdelegate =(FBSliderMenuViewController *)self.menuContainerViewController.leftMenuViewController;
}

#pragma set barbuttons
-(void)setbarbuttons
{
    self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;
    
#pragma left menu bar
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    self.navigationItem.leftBarButtonItems = @[leftbarbutton];
    
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setcartbutton];
#pragma badge circle
    UIImageView *badge=[[UIImageView alloc] initWithFrame:btn2.bounds];
    badge.frame=CGRectMake(16, -3, 20, 20);
    badge.contentMode=UIViewContentModeScaleAspectFit;
    badge.image=[UIImage imageNamed:@"circle_icon.png"];
#pragma number label
    UILabel *badgeno=[[UILabel alloc] initWithFrame:badge.bounds];
    badgeno.frame=CGRectMake(2, 2, 16, 16);
    badgeno.font=[UIFont fontWithName:@"Philosopher" size:12.0f];
    badgeno.textAlignment=NSTextAlignmentCenter;
    badgeno.text=app->totalproduct;
    badgeno.textColor=[UIColor whiteColor];
    [badge addSubview:badgeno];
    // end
    [btn2 addSubview:badge];
    
    [btn2 addTarget:self action:@selector(cartbutton) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)cartbutton
{
    [self.wishdelegate buttonclick];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"WishList"]];
#pragma service call
    if(!loadlist)
    {
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        selectedservice=@"fetchwishlist";
        NSDictionary *param=@{@"id_customer": [app->logindetails valueForKeyPath:@"result.id_customer"]};
        [conn servicecall:param :@"showwishlist.php"];
        
    }
   
}
#pragma reponse handle
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"fetchwishlist"])
    {
    if(!connectiondict)
    {
        return;
    }
    else
    {
        wishlistarray=[[connectiondict valueForKey:@"result"] mutableCopy];
        if(wishlistarray.count==0)
        {
            _lblnowishlist.hidden=NO;
        }
        else
        {
            _lblnowishlist.hidden=YES;
        }
        [self.tblView reloadData];
    }
    }
    else if([selectedservice isEqualToString:@"deletewishlist"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
            [wishlistarray removeObjectAtIndex:deletedindex.row];
            [self.tblView deleteRowsAtIndexPaths:@[deletedindex] withRowAnimation:UITableViewRowAnimationLeft];
            
            if(wishlistarray.count==0)
            {
                _lblnowishlist.hidden=NO;
            }
            else
            {
                _lblnowishlist.hidden=YES;
            }
            
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Deleted" message:[connectiondict valueForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return wishlistarray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"Cell";
    
    WishListTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:cellNameFirst forIndexPath:indexPath];
    cell.lblname.text=[NSString stringWithFormat:@"%@",[wishlistarray[indexPath.row] valueForKey:@"name"]];
    cell.lblprice.text=[NSString stringWithFormat:@"$%.2f",[[wishlistarray[indexPath.row] valueForKey:@"price"] floatValue]];
    
    
    //new code for image load
    NSString *imagepath=[NSString stringWithFormat:@"%@",[wishlistarray[indexPath.row] valueForKey:@"imagelink"]];
    //imagepath=[imagepath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor brownColor];
    activityIndicator.center = CGPointMake(CGRectGetMidX(cell.imgproduct.bounds), CGRectGetMidY(cell.imgproduct.bounds));
    activityIndicator.hidesWhenStopped = YES;
    
    __block BOOL successfetchimage=FALSE;
    __weak UIImageView *setimage = cell.imgproduct;
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
    [cell.imgproduct setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placeholderview.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         setimage.contentMode=UIViewContentModeScaleToFill;
         setimage.image=image;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
     }
     
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator removeFromSuperview];
     }];
    
    
    if(!successfetchimage)
    {
        if(![imagepath isEqualToString:@""]||imagepath)
        {
            [cell.imgproduct addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // NSLog(@"last index path=%ld",(long)lastindexpath.row);
    [self performSegueWithIdentifier:@"pushtoproductdescription" sender:indexPath];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

- (IBAction)didTapDelete:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    deletedindex=indexPath;
    
    UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Do you want to delete product from wishlist?" message:[wishlistarray[indexPath.row] valueForKeyPath:@"name"] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alrt.tag=100;
    [alrt show];
    
   
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
            deletedindex=[[NSIndexPath alloc] init];
        }
        else
        {
            selectedservice=@"deletewishlist";
            NSString *wishlistid=[NSString stringWithFormat:@"%@",[wishlistarray[deletedindex.row] valueForKeyPath:@"id_wishlist_product"]];
            NSDictionary *params=@{@"id_wishlist_product": wishlistid,@"id_customer": [app->logindetails valueForKeyPath:@"result.id_customer"]};
            [conn servicecall:params :@"deletewishlist.php"];
        }
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoproductdescription"])
    {
        NSIndexPath *indexpath=(NSIndexPath *)sender;
        ProductDescriptionViewController *inst=(ProductDescriptionViewController *)segue.destinationViewController;
        inst.selectedproductdict=wishlistarray[indexpath.row];
        
    }
}


@end
