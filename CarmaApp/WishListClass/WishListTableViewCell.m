//
//  WishListTableViewCell.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 04/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "WishListTableViewCell.h"
#import "AppDelegate.h"

@implementation WishListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
