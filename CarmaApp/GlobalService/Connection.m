//
//  Connection.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 25/02/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "Connection.h"
#import "AppDelegate.h"

 AppDelegate *app;


@implementation Connection


-(void)servicecall: (NSDictionary *)params :(NSString *)appendurl
{
    
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *path =[NSString stringWithFormat:@"%@%@",app->parentUrl,appendurl];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
//         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        // NSLog(@"%@",dict);

         //here is place for code executed in success case
         if([[dict valueForKeyPath:@"status"]integerValue]==0)
         {
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:[dict valueForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
#pragma send to dash board
             [self.connectionDelegate sucessdone:dict];
            
         }
        // [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        // [SVProgressHUD dismiss];

         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", error.localizedDescription);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:error.localizedDescription message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}



@end