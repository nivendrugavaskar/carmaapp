//
//  Connection.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 25/02/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol connectionprotocol <NSObject>

-(void)sucessdone:(NSDictionary *)connectiondict;

@end


@interface Connection : NSObject<UIAlertViewDelegate>
{
   
}

-(void)servicecall: (NSDictionary *)params : (NSString *)appendurl;

@property (retain) id<connectionprotocol> connectionDelegate;

@end
