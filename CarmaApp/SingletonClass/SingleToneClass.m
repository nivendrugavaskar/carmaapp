//
//  SingleToneClass.m
//  QuickChat
//
//  Created by Rakesh Jyoti on 06/11/14.
//  Copyright (c) 2014 Raj. All rights reserved.
//

#import "SingleToneClass.h"

@implementation SingleToneClass
{
   
}


+ (id)sharedManager
{
    static SingleToneClass *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.billinginfoInfoDict = [[NSMutableDictionary alloc] init];
        sharedMyManager.shippinginfoInfoDict = [[NSMutableDictionary alloc] init];
    });
    return sharedMyManager;
}

@end
