//
//  SingleToneClass.h
//  QuickChat
//
//  Created by Rakesh Jyoti on 06/11/14.
//  Copyright (c) 2014 Raj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AFNetworking.h"

@interface SingleToneClass : NSObject

+ (id)sharedManager;

@property (weak, nonatomic) NSString *statusText;
@property (strong, nonatomic) UIImage *friendUserImg;

@property NSMutableDictionary *billinginfoInfoDict;
@property NSMutableDictionary *shippinginfoInfoDict;

@property NSDictionary *imgDictAll;



@end
