//
//  SearchProductViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 08/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchProductViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UILabel *noproductfound;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;

@end
