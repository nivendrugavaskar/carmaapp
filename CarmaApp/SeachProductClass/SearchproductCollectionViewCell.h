//
//  SearchproductCollectionViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 08/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchproductCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblproductname;
@property (strong, nonatomic) IBOutlet UILabel *lblprice;
@property (strong, nonatomic) IBOutlet UIImageView *imgviewproduct;

@end
