//
//  SearchProductViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 08/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "SearchProductViewController.h"
#import "AppDelegate.h"
#import "SearchproductCollectionViewCell.h"
#import "ProductDescriptionViewController.h"

@interface SearchProductViewController ()<UISearchBarDelegate,connectionprotocol>
{
    BOOL loadpage;
    AppDelegate *app;
    CGSize kbsize;
    int pagenumber,totalproduct;
    Connection *conn;
    
    NSMutableArray *productArray;
}

@end

@implementation SearchProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setupbarbutton];
    loadpage=FALSE;
    pagenumber=1;
    productArray=[[NSMutableArray alloc] init];
    
    self.collectionView.hidden=TRUE;
    _noproductfound.hidden=NO;
    _noproductfound.text=@"Please enter a product name";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardshow:) name:UIKeyboardDidShowNotification object:nil];
}
- (void)keyboardshow:(NSNotification*)notification
{
    NSDictionary* info = notification.userInfo;
    kbsize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.collectionView.frame=CGRectMake(self.collectionView.frame.origin.x, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, self.collectionView.frame.size.height-kbsize.height);
}


-(void)searchbarresponder
{
    [_searchbar becomeFirstResponder];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Search"]];
     [_searchbar becomeFirstResponder];
    
    if(!loadpage)
    {
        
    }
}
-(void)viewDidAppear:(BOOL)animated
{
   
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}
#pragma searchbar delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [self dismisskeyboard];
    pagenumber=1;
#pragma call service
    productArray=[[NSMutableArray alloc] init];
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    NSDictionary *param=@{@"searchtext": _searchbar.text,@"pagenum": [NSString stringWithFormat:@"%d",pagenumber],@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
    [conn servicecall:param :@"productsearch.php"];
    

}

-(void)dismisskeyboard
{
    [_searchbar resignFirstResponder];
    self.collectionView.frame=CGRectMake(self.collectionView.frame.origin.x, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, self.collectionView.frame.size.height+kbsize.height);
    [_searchbar endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
   // [NSThread cancelPreviousPerformRequestsWithTarget:self selector:@selector(eventlistservice) object:nil];
    
    if(searchText.length>0)
    {
        NSLog(@"seachtext not blank");
        _noproductfound.hidden=YES;
        pagenumber=1;
       // afterscroll=FALSE;
        // seachedarray=[[NSMutableArray alloc] init];
        [self searcheddata];
    }
    else
    {
       // afterscroll=FALSE;
       // seachedarray=[[NSMutableArray alloc] init];
        //_noproductfound.hidden=NO;
       // _noproductfound.text=@"Please enter an event name, venue name, city or state";
       // [self.aTableview reloadData];
    }
    
}
-(void)searcheddata
{
    
    NSString *searchText = _searchbar.text;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    searchText = [searchText stringByTrimmingCharactersInSet:whitespace];
    // new code
    if(searchText.length>3)
    {
        NSLog(@"Service call");
        pagenumber=1;
        self.collectionView.hidden=NO;
        _noproductfound.hidden=YES;
#pragma call service
        productArray=[[NSMutableArray alloc] init];
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        NSDictionary *param=@{@"searchtext": _searchbar.text,@"pagenum": [NSString stringWithFormat:@"%d",pagenumber],@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
        [conn servicecall:param :@"productsearch.php"];
        
    }
    else
    {
        pagenumber=1;
        //self.aTableview.hidden=YES;
        // _norecordfoundlabel.hidden=NO;
    }
    
}

#pragma delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if(scrollView.contentOffset.y+scrollView.frame.size.height==scrollView.contentSize.height)
    {
        
        NSLog(@"call service here");
        pagenumber++;
        if(((pagenumber-1)*10)<=totalproduct)
        {
            
            NSDictionary *param=@{@"searchtext": _searchbar.text,@"pagenum": [NSString stringWithFormat:@"%d",pagenumber],@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
            [conn servicecall:param :@"productsearch.php"];
        }
        
    }
}
#pragma handle response
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
   if(!connectiondict)
   {
       
   }
    else
    {
        NSMutableArray *localarray=[connectiondict valueForKeyPath:@"result"];
        totalproduct=(int)[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"totalproduct"]].integerValue;
        for(int i=0;i<localarray.count;i++)
        {
            [productArray addObject:localarray[i]];
        }
        
        if(productArray.count==0)
        {
            _noproductfound.hidden=NO;
            self.collectionView.hidden=YES;
            _noproductfound.text=@"No product found";
        }
        else
        {
            _noproductfound.hidden=YES;
            self.collectionView.hidden=NO;
        }
        
        [self.collectionView reloadData];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupbarbutton
{
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
    
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setcartbutton];
    [btn2 addTarget:self action:@selector(cartbutton) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)cartbutton
{
    
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionViewDataSource
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismisskeyboard];
    [self performSegueWithIdentifier:@"pushtoproductdescription" sender:indexPath];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return productArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
        SearchproductCollectionViewCell *cell;
        cell =(SearchproductCollectionViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"ListCell" forIndexPath:indexPath];
    
       cell.lblproductname.text=[NSString stringWithFormat:@"%@",[productArray[indexPath.row] valueForKey:@"name"]];
    
      float price=[NSString stringWithFormat:@"%@",[productArray[indexPath.row] valueForKey:@"price"]].floatValue;
    
      cell.lblprice.text=[NSString stringWithFormat:@"$%.2f",price];
    
        //new code for image load
        NSArray *imagearray=[productArray[indexPath.row] valueForKey:@"images"];
        NSString *imagepath=[NSString stringWithFormat:@"%@",imagearray[0]];
        //imagepath=[imagepath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.color=[UIColor brownColor];
        activityIndicator.center = CGPointMake(CGRectGetMidX(cell.imgviewproduct.bounds), CGRectGetMidY(cell.imgviewproduct.bounds));
        activityIndicator.hidesWhenStopped = YES;
        
        __block BOOL successfetchimage=FALSE;
        __weak UIImageView *setimage = cell.imgviewproduct;
        
        
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
        [cell.imgviewproduct setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placeholderview.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             setimage.contentMode=UIViewContentModeScaleToFill;
             setimage.image=image;
             successfetchimage=TRUE;
             [activityIndicator removeFromSuperview];
         }
         
                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             [activityIndicator removeFromSuperview];
         }];
        
        
        if(!successfetchimage)
        {
            if(![imagepath isEqualToString:@""]||imagepath)
            {
                [cell.imgviewproduct addSubview:activityIndicator];
                [activityIndicator startAnimating];
            }
        }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
       float width;
    
        width=self.collectionView.frame.size.width;
        return CGSizeMake(width, 130);
   
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoproductdescription"])
    {
        NSIndexPath *indexpath=(NSIndexPath *)sender;
        ProductDescriptionViewController *inst=(ProductDescriptionViewController *)segue.destinationViewController;
        inst.selectedproductdict=productArray[indexpath.row];
        
    }
}


@end
