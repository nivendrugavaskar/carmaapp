//
//  Hidecursor.m
//  ZoneTickets
//
//  Created by Nivendru on 04/06/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "Hidecursor.h"

@implementation Hidecursor

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGRect) caretRectForPosition:(UITextPosition*) position
{
    return CGRectZero;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return NO;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}

@end
