//
//  Hidecursor.h
//  ZoneTickets
//
//  Created by Nivendru on 04/06/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Hidecursor : UITextField<UITextFieldDelegate>


- (CGRect) caretRectForPosition:(UITextPosition*) position;

@end
