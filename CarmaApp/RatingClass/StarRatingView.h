//
//  StarRatingView.h
//  StarRatingDemo
//
//  Created by HengHong on 5/4/13.
//  Copyright (c) 2013 Fixel Labs Pte. Ltd. All rights reserved.
//


#import <UIKit/UIKit.h>
@protocol startratingprotocol
- (void)sendstarvalue:(int)sender;
@end

@interface StarRatingView : UIView

- (id)initWithFrame:(CGRect)frame andRating:(int)rating withLabel:(BOOL)label animated:(BOOL)animated;

@property (assign) id <startratingprotocol> delegate;
@property int indexx;
//-(void)setstarrvalue:(int)rating;
@end