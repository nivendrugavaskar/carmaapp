//
//  ProductDescriptionViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 17/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ProductDescriptionViewController.h"
#import "AppDelegate.h"
#import "ReviewListViewController.h"

#define kLabelAllowance 50.0f
#define kStarViewHeight 30.0f
#define kStarViewWidth 180.0f
#define kLeftPadding 5.0f

@interface ProductDescriptionViewController ()<connectionprotocol,UIWebViewDelegate,GPPSignInDelegate,GPPShareDelegate>
{
    AppDelegate *app;
    UIImageView *smallimage;
    NSMutableArray *ProductiondescArray,*imagearray,*allimagessmall;
    NSInteger totalSeconds;
    NSTimer *timer;
    int Qtyitem,productquantity;
    NSString *selectedservice;
    Connection *conn;
    TWTRSession *tracksession;
    
    NSString *starcount;
    
    CGPoint pointtrack,pointtrackcontentoffset;
}



@end

@implementation ProductDescriptionViewController

@synthesize keyboardtoolbar;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
    
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setaddtocartbutton:@"Add to Cart"];
    [btn2 addTarget:self action:@selector(addtocart) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
    [SVProgressHUD showWithStatus:@"Please wait.."];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadData];
    });
    
#pragma manage tabview
    for (id v in _tabview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *vw=(UIView *)v;
            vw.backgroundColor=[UIColor clearColor];
        }
    }
    
#pragma new code
   // _tabInfoView.layer.cornerRadius=15.0;
    _tabInfoView.layer.borderWidth=1.0;
    _tabInfoView.layer.borderColor=[UIColor blackColor].CGColor;
    
   // _tabreview.layer.cornerRadius=15.0;
    _tabreview.layer.borderWidth=1.0;
    _tabreview.layer.borderColor=[UIColor blackColor].CGColor;
    
    _txtQtybox.layer.borderColor=[UIColor whiteColor].CGColor;
    _txtQtybox.layer.borderWidth=1.0;
    //_txtQtybox.layer.cornerRadius=5.0;
    _txtQtybox.clipsToBounds=YES;
    
    Qtyitem=1;
    
    productquantity=(int)[NSString stringWithFormat:@"%@",[self.selectedproductdict valueForKeyPath:@"quantity"]].integerValue;
    
}
#pragma webview delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"])
    {
        // UIWebView object has fully loaded.
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
    }
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error for WEBVIEW: %@", error.description);
     [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)slidemenu
{
   [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)addtocart
{
    NSLog(@"Add to cart");
//    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Added to Cart" message:@"Successfully added to your Cart" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    selectedservice=@"FetchCart";
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    //id_product,quantity,id_customer
    NSDictionary *param=@{@"id_customer": [app->logindetails valueForKeyPath:@"result.id_customer"],@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"],@"id_product": [self.selectedproductdict valueForKeyPath:@"id_product"],@"quantity": _txtQtybox.text};
    [conn servicecall:param :@"addtocartLoginuser.php"];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
      [self performSegueWithIdentifier:@"pushtocheckoutpage" sender:self];
    }
    else if (alertView.tag==103)
    {
       // [self didTapTwitter:nil];
        [self performSelector:@selector(twittershare) withObject:self afterDelay:1.0];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Optional
-(void)timerstart
{
    // new code
    totalSeconds = 0; //6 hours * 60 minutes * 60 seconds
    timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(updatepicture:) userInfo:nil repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}
-(void)dynamiccontentset
{
    NSLog(@"%ld",(long)totalSeconds);
    self.smallscroll.contentOffset=CGPointMake(90*(totalSeconds-3), 0);
    
}
-(void)updatepicture:(NSTimer *) timer1
{
    // CGPoint point=self.smallscroll.contentOffset;
    
    NSLog(@"%f",timer1.timeInterval);
    NSLog(@"%ld",(long)totalSeconds);
    for(UIImageView *v in self.smallscroll.subviews)
    {
        if(v.tag==totalSeconds)
        {
            (v.layer).borderWidth = 2.0;
            (v.layer).borderColor = [UIColor whiteColor].CGColor;
            
        }
        else
        {
            (v.layer).borderWidth = 0.0;
            (v.layer).borderColor = [UIColor clearColor].CGColor;
        }
        
    }
    self.smallscroll.contentOffset=CGPointMake(0, 0);
    // self.bigimage.image=[imagearray objectAtIndex:totalSeconds];
//    [self.bigimage setImageWithURL:[NSURL URLWithString:[imagearray objectAtIndex:totalSeconds]]
//                  placeholderImage:[UIImage imageNamed:@"placeholderview.png"]];
    
    self.bigimage.image=imagearray[totalSeconds];
    totalSeconds=totalSeconds+1;
    if(totalSeconds>3)
    {
        [self dynamiccontentset];
    }
    if(totalSeconds==imagearray.count)
    {
        totalSeconds=0;
    }
}

-(void)loadData
{
    NSLog(@"Selectedproduct=%@",self.selectedproductdict);
#pragma set product info
    if([[self.selectedproductdict valueForKey:@"reviews"] isEqualToString:@"0"])
    {
     _lblreviewnumber.text=@"No Review(s)";
    }
    else
    {
    _lblreviewnumber.text=[NSString stringWithFormat:@"%@ Review(s)",[self.selectedproductdict valueForKey:@"reviews"]];
    }
    
    if([[NSString stringWithFormat:@"%@",[self.selectedproductdict valueForKey:@"wishlist"]] isEqualToString:@"1"])
    {
        self.wishlistimage.image=[UIImage imageNamed:@"selectedwishlist.png"];
    }
    else
    {
         self.wishlistimage.image=[UIImage imageNamed:@"wishlist_icon.png"];
    }
    
    
    _lblproductname.text=[NSString stringWithFormat:@"%@",[self.selectedproductdict valueForKey:@"name"]];
    float price=[NSString stringWithFormat:@"%@",[self.selectedproductdict valueForKey:@"price"]].floatValue;
    _lblproductprice.text=[NSString stringWithFormat:@"$%.2f",price];
    
    // for description
    //CGPoint point=self.smallscroll.contentOffset;
    
    //[SVProgressHUD showWithStatus:@"Loading"];
  //  [self.showcontentWebview loadHTMLString:[self.scenarioDescriptionDict valueForKey:@"description"] baseURL:nil];
    
    imagearray=[[NSMutableArray alloc]init];
    allimagessmall=[[NSMutableArray alloc] init];
    allimagessmall=[self.selectedproductdict valueForKey:@"images"];
    
//    allimagessmall=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"1.jpg"],[UIImage imageNamed:@"2.jpg"],[UIImage imageNamed:@"3.jpg"],[UIImage imageNamed:@"4.jpg"],[UIImage imageNamed:@"5.jpg"],[UIImage imageNamed:@"6.jpg"], nil];
    
    
    
    //    // new code
    //    for(int i=0;i<[scenarioDescArray count];i++)
    //    {
    //        NSString *imagenilcheck=[[scenarioDescArray objectAtIndex:i]valueForKey:@"smallimages_name"];
    //        //NSData *urldata;
    //        //urldata=[self sendimagedata:imagenilcheck];
    //       // UIImage *urlimage=[[UIImage alloc]initWithData:urldata];
    //        if(imagenilcheck)
    //        {
    //            [allimagessmall addObject:imagenilcheck];
    //        }
    //        NSString *imageurl1=[[scenarioDescArray objectAtIndex:i]valueForKey:@"bigimages_name"];
    //       // NSData *urldata1;
    //       // urldata1=[self sendimagedata:imageurl1];
    //       // UIImage *urlimage1=[[UIImage alloc]initWithData:urldata1];
    //        if(imageurl1)
    //        {
    //            [imagearray addObject:imageurl1];
    //        }
    //    }
    //
    
    //
    //    if([allimagessmall count]==0)
    //    {
    //        [SVProgressHUD dismiss];
    //        _smallscroll.hidden=YES;
    //        _bigimage.hidden=YES;
    //        _leftArrowimage.hidden=YES;
    //        _rightArrowimage.hidden=YES;
    //        _noimageimageview.hidden=NO;
    //        _noimagelabel.hidden=NO;
    //
    //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Images" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //        [alert show];
    //        return;
    //    }
    //    else
    //    {
    //        _noimageimageview.hidden=YES;
    //        _noimagelabel.hidden=YES;
    //    }
    
    //end
    
   // float thumbsize=(((self.smallscroll.frame.size.width)-40)/3);
    //float thumbheight=((self.smallscroll.frame.size.height));

    
    
    self.smallscroll.contentSize=CGSizeMake((80*6)+10*(6+1), 0);
    
    for (int i=0; i<allimagessmall.count; i++)
    {
        smallimage=[[UIImageView alloc]initWithFrame:CGRectMake(10+(i*80)+(10*i), 2, 80, 80)];
        smallimage.clipsToBounds=YES;
        smallimage.contentMode = UIViewContentModeScaleAspectFit;
        smallimage.backgroundColor=[UIColor clearColor];
        
        // for small image
        NSString *imageurl=allimagessmall[i];
        [imagearray addObject:allimagessmall[i]];
       
        [smallimage setImageWithURL:[NSURL URLWithString:imageurl]
                       placeholderImage:[UIImage imageNamed:@"placeholderview.png"]];
        
            if(i==0)
            {
                (smallimage.layer).borderColor = [UIColor whiteColor].CGColor;
                (smallimage.layer).borderWidth = 2.0;
              [self.bigimage setImageWithURL:[NSURL URLWithString:imagearray[0]]
                              placeholderImage:[UIImage imageNamed:@"placeholderview.png"]];
                
            }
        smallimage.tag=i;
        // add gesture
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        singleTap.delegate = (id)self;
        [smallimage addGestureRecognizer:singleTap];
        smallimage.userInteractionEnabled = YES;
        // end
        [self.smallscroll addSubview:smallimage];
    }
    if(self.smallscroll.contentSize.width>self.smallscroll.frame.size.width)
    {
        self.leftArrowimage.hidden=TRUE;
    }
    else
    {
        self.leftArrowimage.hidden=TRUE;
        self.rightArrowimage.hidden=TRUE;
    }
    
    [SVProgressHUD dismiss];
    
   // [self viewWillAppear:YES];
    //[self timerstart];
    
}

-(void)oneTap:(UIGestureRecognizer *)recognizer
{
    
    UIImageView *img = (UIImageView *)recognizer.view;
   // NSLog(@"imageviewtag=%ld",(long)img.tag);
    for(UIImageView *v in self.smallscroll.subviews)
    {
        (v.layer).borderWidth = 0.0;
        (v.layer).borderColor = [UIColor clearColor].CGColor;
    }
    (img.layer).borderColor = [UIColor whiteColor].CGColor;
    (img.layer).borderWidth = 2.0;
    
    [self.bigimage setImageWithURL:[NSURL URLWithString:imagearray[img.tag]]
                  placeholderImage:[UIImage imageNamed:@"placeholderview.png"]];
    
    CGFloat contentOffsetX = self.smallscroll.contentOffset.x;
    
    if((img.frame.origin.x+img.frame.size.width)>self.smallscroll.frame.size.width+contentOffsetX)
    {
    self.smallscroll.contentOffset=CGPointMake(((img.frame.origin.x+img.frame.size.width)-self.smallscroll.frame.size.width)+10, 0);
    }
    else
    {
      self.smallscroll.contentOffset=CGPointMake(contentOffsetX, 0);
    }
    [self.bigimage setImageWithURL:[NSURL URLWithString:imagearray[img.tag]]
                  placeholderImage:[UIImage imageNamed:@"placeholderview.png"]];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat contentOffsetX = scrollView.contentOffset.x;
  //  NSLog(@"scrollviewcontentX=%.2f",contentOffsetX);
    CGFloat size=scrollView.contentSize.width;
   // NSLog(@"scrollviewcontentwidth=%.2f",size);
    int fetchCount=size-contentOffsetX;
    if(fetchCount<=self.smallscroll.frame.size.width)
    {
        self.rightArrowimage.hidden=TRUE;
        self.leftArrowimage.hidden=FALSE;
    }
    else
    {
        if(fetchCount>=scrollView.contentSize.width)
        {
            self.leftArrowimage.hidden=TRUE;
            self.rightArrowimage.hidden=FALSE;
        }
        else
        {
            self.leftArrowimage.hidden=FALSE;
            self.rightArrowimage.hidden=FALSE;
        }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:[self.selectedproductdict valueForKey:@"name"]]];
    
    //    if([imagearray count]>1)
    //    {
    //        [self timerstart];
    //    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoreviewlist"])
    {
        ReviewListViewController *inst=(ReviewListViewController *)segue.destinationViewController;
        inst.productdict=self.selectedproductdict;
    }
}


- (IBAction)didTapminusclick:(id)sender {
    NSLog(@"Minusclick");
    
    if(Qtyitem>1)
    {
        Qtyitem--;
        _txtQtybox.text=[NSString stringWithFormat:@"%d",Qtyitem];
        #pragma dyanamic price set
        float finalprice=Qtyitem*[NSString stringWithFormat:@"%@",[self.selectedproductdict valueForKey:@"price"]].floatValue;
        _lblproductprice.text=[NSString stringWithFormat:@"$%.2f",finalprice];
    }
}

- (IBAction)didTapPlusClick:(id)sender
{
     NSLog(@"Plusclick");
    if(Qtyitem==productquantity)
    {
        UIAlertView *alt=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Max limit reached for this product" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alt show];
    }
    else
    {
    Qtyitem++;
    _txtQtybox.text=[NSString stringWithFormat:@"%d",Qtyitem];
#pragma dyanamic price set
    float finalprice=Qtyitem*[NSString stringWithFormat:@"%@",[self.selectedproductdict valueForKey:@"price"]].floatValue;
    _lblproductprice.text=[NSString stringWithFormat:@"$%.2f",finalprice];
    }
    
    
}
#pragma tab bar buttons
- (IBAction)didTapShippingTerm:(id)sender
{
     self.activebutton.userInteractionEnabled=YES;
    self.activebutton=(UIButton *)sender;
    
    
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    if(!_tabInfoView.hidden)
    {
        [self didTapDismiss:nil];
    }
    if(!_tabreview.hidden)
    {
        [self didTapclose:nil];
    }
    
    UIButton *btn=(UIButton *)sender;
    
    for (id v in _tabview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *vw=(UIView *)v;
            if(vw.tag==13)
            {
             vw.backgroundColor=[UIColor colorWithRed:0/255.0f green:75/255.0f blue:1/255.0f alpha:1.0f];
                _lblshippingterms.textColor=[UIColor colorWithRed:0/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f];
            }
            else
            {
                vw.backgroundColor=[UIColor clearColor];
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UIButton class]])
                    {
                        UIButton *bt=(UIButton *)v;
                        if(bt==btn)
                        {
                            
                        }
                        else
                        {
                            [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        }
                    }
                }
                
            }
            
        }
    }
    
    _tabInfoView.hidden=FALSE;
    _tabInfoView.alpha=0.0;
    [UIView animateWithDuration:0.25 animations:^{
        _infotitle.text=_lblshippingterms.text;
        _tabInfoView.alpha=1.0;
        
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [SVProgressHUD showWithStatus:@"Loading.."];
        [self.contentwebview loadHTMLString:[self.selectedproductdict valueForKey:@"shipcontent"] baseURL:nil];
    }];
    
    self.activebutton.userInteractionEnabled=NO;
    
}
- (IBAction)didTapReviews:(id)sender
{
    _txtreview.inputAccessoryView=keyboardtoolbar;
    _txtreviewtitle.inputAccessoryView=keyboardtoolbar;
    _txtreview.text=@"";
    _txtreviewtitle.text=@"";
    
#pragma set fame to manage animation
    pointtrackcontentoffset=self.tabreview.frame.origin;
    
    
     self.activebutton.userInteractionEnabled=YES;
    self.activebutton=(UIButton *)sender;
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
    if(!_tabInfoView.hidden)
    {
        [self didTapDismiss:nil];
    }
    if(!_tabreview.hidden)
    {
        [self didTapclose:nil];
    }
    
    UIButton *btn=(UIButton *)sender;
    
    for (id v in _tabview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *vw=(UIView *)v;
            if(vw.tag==14)
            {
                vw.backgroundColor=[UIColor colorWithRed:0/255.0f green:75/255.0f blue:1/255.0f alpha:1.0f];
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UIButton class]])
                    {
                        UIButton *bt=(UIButton *)v;
                        [bt setTitleColor:[UIColor colorWithRed:0/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f] forState:UIControlStateNormal];
                    }
                }
            }
            else
            {
                vw.backgroundColor=[UIColor clearColor];
                if(vw.tag==13)
                {
                    _lblshippingterms.textColor=[UIColor blackColor];
                }
                else
                {
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UIButton class]])
                    {
                        UIButton *bt=(UIButton *)v;
                        if(bt==btn)
                        {
                            
                        }
                        else
                        {
                            [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        }
                    }
                }
            }
            
            }
            
        }
    }
    
    _tabreview.hidden=FALSE;
    _tabreview.alpha=0.0;
    [UIView animateWithDuration:0.25 animations:^{
        // new code

        _tabreview.alpha=1.0;
        _txtreview.layer.borderColor=[UIColor blackColor].CGColor;
        _txtreview.layer.borderWidth=1.0;
        self.ratingView.backgroundColor=[UIColor clearColor];
        self.ratingView.delegate=self;
#pragma starcount=3
        starcount=@"3";
        self.ratingView=[self.ratingView initWithFrame:self.ratingView.frame andRating:20*[starcount integerValue] withLabel:NO animated:NO];
    }];
    
    self.activebutton.userInteractionEnabled=NO;
}
#pragma star rating protocol
-(void)sendstarvalue:(int)sender
{
     NSLog(@"star count=%d",sender);
    int starnumber=sender/20;
    starcount=[NSString stringWithFormat:@"%d",starnumber];
    self.ratingView=[self.ratingView initWithFrame:self.ratingView.frame andRating:sender withLabel:NO animated:NO];
}

- (IBAction)didTapMaintanance:(id)sender {
    
     self.activebutton.userInteractionEnabled=YES;
    self.activebutton=(UIButton *)sender;
    
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
    if(!_tabInfoView.hidden)
    {
        [self didTapDismiss:nil];
    }
    if(!_tabreview.hidden)
    {
         [self didTapclose:nil];
    }
    
    UIButton *btn=(UIButton *)sender;
    
    for (id v in _tabview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *vw=(UIView *)v;
            if(vw.tag==12)
            {
                vw.backgroundColor=[UIColor colorWithRed:0/255.0f green:75/255.0f blue:1/255.0f alpha:1.0f];
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UIButton class]])
                    {
                        UIButton *bt=(UIButton *)v;
                        [bt setTitleColor:[UIColor colorWithRed:0/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f] forState:UIControlStateNormal];
                    }
                }
            }
            else
            {
                vw.backgroundColor=[UIColor clearColor];
                if(vw.tag==13)
                {
                    _lblshippingterms.textColor=[UIColor blackColor];
                }
                else
                {
                    for (id v in vw.subviews)
                    {
                        if([v isKindOfClass:[UIButton class]])
                        {
                            UIButton *bt=(UIButton *)v;
                            if(bt==btn)
                            {
                                
                            }
                            else
                            {
                                [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            }
                        }
                    }
                }
                
            }
            
        }
    }
    
    _tabInfoView.hidden=FALSE;
    _tabInfoView.alpha=0.0;
    [UIView animateWithDuration:0.25 animations:^{
        _infotitle.text=btn.titleLabel.text;
        _tabInfoView.alpha=1.0;
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [SVProgressHUD showWithStatus:@"Loading.."];
        [self.contentwebview loadHTMLString:[self.selectedproductdict valueForKey:@"maintainancecontent"] baseURL:nil];
    }];
    
    self.activebutton.userInteractionEnabled=NO;
}
- (IBAction)didTapInstallation:(id)sender {
    
    self.activebutton.userInteractionEnabled=YES;
    self.activebutton=(UIButton *)sender;
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
    if(!_tabInfoView.hidden)
    {
        [self didTapDismiss:nil];
    }
    if(!_tabreview.hidden)
    {
         [self didTapclose:nil];
    }
    
    UIButton *btn=(UIButton *)sender;
    
    for (id v in _tabview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *vw=(UIView *)v;
            if(vw.tag==11)
            {
                vw.backgroundColor=[UIColor colorWithRed:0/255.0f green:75/255.0f blue:1/255.0f alpha:1.0f];
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UIButton class]])
                    {
                        UIButton *bt=(UIButton *)v;
                        [bt setTitleColor:[UIColor colorWithRed:0/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f] forState:UIControlStateNormal];
                    }
                }
            }
            else
            {
                vw.backgroundColor=[UIColor clearColor];
                if(vw.tag==13)
                {
                    _lblshippingterms.textColor=[UIColor blackColor];
                }
                else
                {
                    for (id v in vw.subviews)
                    {
                        if([v isKindOfClass:[UIButton class]])
                        {
                            UIButton *bt=(UIButton *)v;
                            if(bt==btn)
                            {
                                
                            }
                            else
                            {
                                [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            }
                        }
                    }
                }
                
            }
            
        }
    }
    
    _tabInfoView.hidden=FALSE;
    _tabInfoView.alpha=0.0;
    [UIView animateWithDuration:0.25 animations:^{
        _infotitle.text=btn.titleLabel.text;
        _tabInfoView.alpha=1.0;
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [SVProgressHUD showWithStatus:@"Loading.."];
        [self.contentwebview loadHTMLString:[self.selectedproductdict valueForKey:@"installationcontent"] baseURL:nil];
    }];
    
    self.activebutton.userInteractionEnabled=NO;
}

- (IBAction)didTapmoreinfo:(id)sender {
    
     self.activebutton.userInteractionEnabled=YES;
    self.activebutton=(UIButton *)sender;
    
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
    if(!_tabInfoView.hidden)
    {
        [self didTapDismiss:nil];
    }
    if(!_tabreview.hidden)
    {
         [self didTapclose:nil];
    }
    
    UIButton *btn=(UIButton *)sender;
    
    for (id v in _tabview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *vw=(UIView *)v;
            if(vw.tag==10)
            {
                vw.backgroundColor=[UIColor colorWithRed:0/255.0f green:75/255.0f blue:1/255.0f alpha:1.0f];
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UIButton class]])
                    {
                        UIButton *bt=(UIButton *)v;
                        [bt setTitleColor:[UIColor colorWithRed:0/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f] forState:UIControlStateNormal];
                    }
                }
            }
            else
            {
                vw.backgroundColor=[UIColor clearColor];
                if(vw.tag==13)
                {
                    _lblshippingterms.textColor=[UIColor blackColor];
                }
                else
                {
                    for (id v in vw.subviews)
                    {
                        if([v isKindOfClass:[UIButton class]])
                        {
                            UIButton *bt=(UIButton *)v;
                            if(bt==btn)
                            {
                                
                            }
                            else
                            {
                                [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            }
                        }
                    }
                }
                
            }
            
        }
    }
    _tabInfoView.hidden=FALSE;
    _tabInfoView.alpha=0.0;
    [UIView animateWithDuration:0.25 animations:^{
        _infotitle.text=btn.titleLabel.text;
        _tabInfoView.alpha=1.0;
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [SVProgressHUD showWithStatus:@"Loading.."];
        [self.contentwebview loadHTMLString:[self.selectedproductdict valueForKey:@"description"] baseURL:nil];
        
    }];
    
    self.activebutton.userInteractionEnabled=NO;
}

- (IBAction)didTapDismiss:(id)sender {
    
    self.activebutton.userInteractionEnabled=YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
#pragma manage tabview
    for (id v in _tabview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *vw=(UIView *)v;
            vw.backgroundColor=[UIColor clearColor];
            if(vw.tag==13)
            {
                vw.backgroundColor=[UIColor clearColor];
                 _lblshippingterms.textColor=[UIColor blackColor];
                
            }
            else
            {
                vw.backgroundColor=[UIColor clearColor];
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UIButton class]])
                    {
                        UIButton *bt=(UIButton *)v;
                        [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    }
                }
            }
        }
    }
    _tabInfoView.hidden=YES;
}
- (IBAction)didTapSend:(id)sender {
    
    
    
    if(![NSString validation:_txtreviewtitle.text])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please enter review title" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if(![NSString validation:_txtreview.text])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please enter review" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    self.activebutton.userInteractionEnabled=YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
    
#pragma service call
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    selectedservice=@"submitreview";
    //id_product,id_customer,title,content,customer_name,grade
    NSMutableDictionary *params=[NSMutableDictionary dictionaryWithObjectsAndKeys:[_selectedproductdict valueForKey:@"id_product"],@"id_product",_txtreviewtitle.text,@"title",[app->logindetails valueForKeyPath:@"result.id_customer"],@"id_customer",_txtreview.text,@"content",starcount,@"grade",[NSString stringWithFormat:@"%@ %@",[app->logindetails valueForKeyPath:@"result.firstname"],[app->logindetails valueForKeyPath:@"result.lastname"]],@"customer_name", nil];
    [conn servicecall:params :@"writereview.php"];
    
//#pragma manage tabview
//    for (id v in _tabview.subviews)
//    {
//        if([v isKindOfClass:[UIView class]])
//        {
//            UIView *vw=(UIView *)v;
//            vw.backgroundColor=[UIColor clearColor];
//            if(vw.tag==13)
//            {
//                vw.backgroundColor=[UIColor clearColor];
//                _lblshippingterms.textColor=[UIColor blackColor];
//                
//            }
//            else
//            {
//                vw.backgroundColor=[UIColor clearColor];
//                for (id v in vw.subviews)
//                {
//                    if([v isKindOfClass:[UIButton class]])
//                    {
//                        UIButton *bt=(UIButton *)v;
//                        [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                    }
//                }
//            }
//        }
//    }
//    _tabreview.hidden=YES;
}

-(IBAction)didTapclose:(id)sender
{
    self.activebutton.userInteractionEnabled=YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
#pragma manage tabview
    for (id v in _tabview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *vw=(UIView *)v;
            vw.backgroundColor=[UIColor clearColor];
            if(vw.tag==13)
            {
                vw.backgroundColor=[UIColor clearColor];
                _lblshippingterms.textColor=[UIColor blackColor];
                
            }
            else
            {
                vw.backgroundColor=[UIColor clearColor];
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UIButton class]])
                    {
                        UIButton *bt=(UIButton *)v;
                        [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    }
                }
            }
        }
    }
     _tabreview.hidden=YES;
    
}
- (IBAction)didTapaddtowishlist:(id)sender {
    selectedservice=@"wishlist";
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    //id_product,quantity,id_customer
    NSMutableDictionary *params=[NSMutableDictionary dictionaryWithObjectsAndKeys:[_selectedproductdict valueForKey:@"id_product"],@"id_product",@"1",@"quantity",[app->logindetails valueForKeyPath:@"result.id_customer"],@"id_customer", nil];
    [conn servicecall:params :@"addtowishlist.php"];
}
#pragma handle response
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"wishlist"])
    {
       if(!connectiondict)
       {
           return;
       }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sucess" message:[connectiondict valueForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            self.wishlistimage.image=[UIImage imageNamed:@"selectedwishlist.png"];
            
            
        }
    }
    else if([selectedservice isEqualToString:@"FetchCart"])
    {
        
        if(!connectiondict)
        {
            return;
        }
        else
        {
           UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:[connectiondict valueForKey:@"msg"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alrt.tag=100;
            [alrt show];
            
            app->totalproduct=[[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"total_product_in_cart"]] mutableCopy];
            [[NSUserDefaults standardUserDefaults] setValue:app->totalproduct forKey:@"cartbadge"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    else if ([selectedservice isEqualToString:@"submitreview"])
    {
        
       
        
#pragma manage tabview
        for (id v in _tabview.subviews)
        {
            if([v isKindOfClass:[UIView class]])
            {
                UIView *vw=(UIView *)v;
                vw.backgroundColor=[UIColor clearColor];
                if(vw.tag==13)
                {
                    vw.backgroundColor=[UIColor clearColor];
                    _lblshippingterms.textColor=[UIColor blackColor];
                    
                }
                else
                {
                    vw.backgroundColor=[UIColor clearColor];
                    for (id v in vw.subviews)
                    {
                        if([v isKindOfClass:[UIButton class]])
                        {
                            UIButton *bt=(UIButton *)v;
                            [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        }
                    }
                }
            }
        }
        _tabreview.hidden=YES;
        
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:[connectiondict valueForKey:@"result"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
}
- (IBAction)didtapfb:(id)sender {
    
    NSLog(@"dict=%@",_selectedproductdict);
    
    [self shareViaFB];
}
#pragma mark - Shareing FB
-(void)shareViaFB{
    
    [SVProgressHUD showWithStatus:@"Please wait.."];
#pragma fetch friends
    if (![FBSDKAccessToken currentAccessToken])
    {
        FBSDKLoginManager *login=[[FBSDKLoginManager alloc] init];
         [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             [SVProgressHUD dismiss];
             if (error)
             {
                 UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Some Error" message:error.description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
                 // Process error
             } else if (result.isCancelled) {
                 // Handle cancellations
                 UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Cancelled" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
             } else {
                 // If you ask for multiple permissions at once, you
                 // should check if specific permissions missing
                 if ([result.grantedPermissions containsObject:@"email"])
                 {
                     FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                     //content.contentURL = [NSURL URLWithString:@"http://www.carmaliving.com/"];
                     content.contentTitle=[_selectedproductdict valueForKey:@"name"];
                     NSString *descstring=[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%.2f",[[_selectedproductdict valueForKey:@"price"] floatValue]]];
                     content.contentDescription=descstring;
                     NSArray *imagecontentarray=[[_selectedproductdict valueForKey:@"images"] mutableCopy];
                     NSString *imageurl=imagecontentarray[0];
                      content.imageURL=[NSURL URLWithString:imageurl];
                     [FBSDKShareDialog showFromViewController:self
                                                  withContent:content
                                                     delegate:nil];
                     
                 }
             }
         }];
    }
    else
    {
         [SVProgressHUD dismiss];
        
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        //content.contentURL = [NSURL URLWithString:@"http://www.carmaliving.com/"];
        content.contentTitle=[_selectedproductdict valueForKey:@"name"];
        NSString *descstring=[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%.2f",[[_selectedproductdict valueForKey:@"price"] floatValue]]];
        content.contentDescription=descstring;
        NSArray *imagecontentarray=[[_selectedproductdict valueForKey:@"images"] mutableCopy];
        NSString *imageurl=imagecontentarray[0];
        content.imageURL=[NSURL URLWithString:imageurl];
        [FBSDKShareDialog showFromViewController:self
                                     withContent:content
                                        delegate:nil];
        
    }
}
// old code
-(void) postImageWithMessageOnTwitter
{
    NSArray *imagecontentarray=[[_selectedproductdict valueForKey:@"images"] mutableCopy];
    NSString *imageurl=imagecontentarray[0];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageurl]];
    //    //NSLog(@"imge %@",imageData);
    UIImage *image = [UIImage imageWithData:imageData];
    SLComposeViewController *mySLComposerSheet;
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [mySLComposerSheet setInitialText:[NSString stringWithFormat:[_selectedproductdict valueForKey:@"name"],mySLComposerSheet.serviceType]]; //the message you want to post
        [mySLComposerSheet addImage:image];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"You have not added twitter account in settings" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    mySLComposerSheet.completionHandler = ^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        } //check if everything worked properly. Give out a message on the state.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    };
}


- (IBAction)didTapTwitter:(id)sender
{
    NSArray *imagecontentarray=[[_selectedproductdict valueForKey:@"images"] mutableCopy];
    NSString *imageurl=[NSString stringWithFormat:@"%@",imagecontentarray[0]];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageurl]];
    //    //NSLog(@"imge %@",imageData);
    UIImage *image = [UIImage imageWithData:imageData];
    SLComposeViewController *mySLComposerSheet;
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [mySLComposerSheet setInitialText:[NSString stringWithFormat:[_selectedproductdict valueForKey:@"name"],mySLComposerSheet.serviceType]]; //the message you want to post
        [mySLComposerSheet addImage:image];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        
        
//        //[self didTapTwitter:sender];
        if(IS_OS_8_OR_LATER)
        {
        [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error)
         {
             if (session)
             {
                 NSLog(@"signed in as %@", [session userName]);
                 //[self didTapTwitter:sender];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Authenticated" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                 alert.tag=103;
                 [alert show];
                 
             }
             else
             {
                 [SVProgressHUD dismiss];
                 NSLog(@"error: %@", [error localizedDescription]);
             }
         }];
        }
        else
        {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"You have not added twitter account in settings" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
        }
    }
    mySLComposerSheet.completionHandler = ^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        } //check if everything worked properly. Give out a message on the state.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    };
    
}
-(void)twittershare
{
   
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    [composer setText:[NSString stringWithFormat:@"%@",[_selectedproductdict valueForKey:@"name"]]];
#pragma custom setup
    NSArray *imagecontentarray=[[_selectedproductdict valueForKey:@"images"] mutableCopy];
    NSString *imageurl=imagecontentarray[0];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageurl]];
        //NSLog(@"imge %@",imageData);
    UIImage *image = [UIImage imageWithData:imageData];
    
    [composer setImage:image];
    
     [SVProgressHUD dismiss];
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        
        
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
            NSLog(@"Sending Tweet!");
        }
    }];
}
- (IBAction)didTapGoogleplus:(id)sender
{
      [SVProgressHUD showWithStatus:@"Loading...."];
     //Use the native share dialog in your app:
        if ([GPPSignIn sharedInstance].authentication)
        {
            [GPPShare sharedInstance].delegate = self;
            NSArray *imagecontentarray=[[_selectedproductdict valueForKey:@"images"] mutableCopy];
            NSString *imageurl=imagecontentarray[0];
            id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
            
            [shareBuilder setContentDeepLinkID:kClientId];
            [shareBuilder setTitle:[_selectedproductdict valueForKey:@"name"] description:nil thumbnailURL:[NSURL URLWithString:imageurl]];
            
            [SVProgressHUD dismiss];
            [shareBuilder open];
        }
        else
        {
            
        GPPSignIn *signIn = [GPPSignIn sharedInstance];
        signIn.shouldFetchGooglePlusUser = YES;
        signIn.clientID =kClientId;
            //@"759096274763-50rvojkti9jflq3526tsrtom25p4heoe.apps.googleusercontent.com";
        signIn.scopes = @[kGTLAuthScopePlusLogin];
        signIn.delegate = self;
        [signIn authenticate];
        }
    
    
}
- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    if(error==nil)
    {
        NSArray *imagecontentarray=[[_selectedproductdict valueForKey:@"images"] mutableCopy];
        NSString *imageurl=imagecontentarray[0];
        
        [GPPShare sharedInstance].delegate = self;
        
        id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
        [shareBuilder setContentDeepLinkID:kClientId];
        [shareBuilder setTitle:[_selectedproductdict valueForKey:@"name"] description:nil thumbnailURL:[NSURL URLWithString:imageurl]];
        [SVProgressHUD dismiss];
        [shareBuilder open];
    }
}

-(void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
     finishedWithAuth:(GTMOAuth2Authentication *)authResult
                error:(NSError *)error
{
    NSLog(@"auth");
}
- (void)finishedSharingWithError:(NSError *)error
{
    NSString *text;
    
    if (!error) {
        text = @"Successfully shared";
    } else if (error.code == kGPPErrorShareboxCanceled) {
   
        text = @"Canceled by user";
    } else {
        text = [NSString stringWithFormat:@"Error (%@)", error.localizedDescription];
    }
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:text message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (IBAction)didtappintrest:(id)sender
{
//    NSArray *imagecontentarray=[[_selectedproductdict valueForKey:@"images"] mutableCopy];
//    NSString *imageurl=imagecontentarray[0];
//    [_pinterest createPinWithImageURL:[NSURL URLWithString:@"http://placehold.it/350x150"] sourceURL:[NSURL URLWithString:@"http://dev.businessprodemo.com/Carmaliving/php2/"] description:@"Test"];
    
//    NSURL *imageURL     = [NSURL URLWithString:@"http://placehold.it/350x150"];
//    NSURL *sourceURL    = [NSURL URLWithString:@"http://placekitten.com"];
//    
//    
//    [app.pinterest createPinWithImageURL:imageURL
//                           sourceURL:sourceURL
//                         description:@"Pinning from Pin It Demo"];
    
   
}

#pragma text field delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack=textField.superview.frame.origin;
    CGRect rect=self.tabreview.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.tabreview.frame=rect;
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self dismisskeyboard];
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.activeTextView=textView;
    
    pointtrack = textView.superview.frame.origin;
    
    CGRect rect=self.tabreview.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(SCREEN_HEIGHT==480||SCREEN_HEIGHT==568)
    {
        if(pointtrack.y>90)
        {
            rect.origin.y-=pointtrack.y+30;
        }
    }
    
    else if(SCREEN_HEIGHT==667)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-100;
        }
    }
    else if(SCREEN_HEIGHT==736)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-120;
        };
    }
    else
    {
        
    }
    self.tabreview.frame=rect;
    [UIView commitAnimations];
    
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
       
    }
    return YES;
}

- (IBAction)didTapReview:(id)sender
{
    [self performSegueWithIdentifier:@"pushtoreviewlist" sender:self];
}
- (IBAction)didTapDone:(id)sender {
    
    [self dismisskeyboard];
}
#pragma dismiss keyboard
-(void)dismisskeyboard
{
    [self.activeTextField resignFirstResponder];
    [self.activeTextView resignFirstResponder];
    CGRect rect=self.tabreview.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.tabreview.frame=rect;
    [UIView commitAnimations];
}
@end

