//
//  ProductDescriptionViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 17/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarRatingView.h"


@interface ProductDescriptionViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate,UITextViewDelegate,startratingprotocol>
@property (strong, nonatomic) IBOutlet UIScrollView *smallscroll;
@property (strong, nonatomic) IBOutlet UIImageView *bigimage;
@property (strong, nonatomic) IBOutlet UIImageView *leftArrowimage;
@property (strong, nonatomic) IBOutlet UIImageView *rightArrowimage;
- (IBAction)didTapminusclick:(id)sender;
- (IBAction)didTapPlusClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *tabview;
@property (strong, nonatomic) IBOutlet UIButton *btnmoreinfo;
@property (strong, nonatomic) IBOutlet UIButton *btninstallation;
@property (strong, nonatomic) IBOutlet UIButton *btnmaintanace;
@property (strong, nonatomic) IBOutlet UILabel *lblshippingterms;
@property (strong, nonatomic) IBOutlet UIButton *btnreview;
@property (strong, nonatomic) IBOutlet UIView *tabInfoView;
@property (strong, nonatomic) IBOutlet UIView *tabreview;
@property (strong, nonatomic) IBOutlet UILabel *infotitle;
@property (strong, nonatomic) IBOutlet UITextField *txtQtybox;
@property (strong, nonatomic) IBOutlet UILabel *lblproductname;
@property (strong, nonatomic) IBOutlet UILabel *lblproductprice;
@property NSMutableDictionary *selectedproductdict;
@property (strong, nonatomic) IBOutlet UIWebView *contentwebview;

@property UIButton *activebutton;
@property (strong, nonatomic) IBOutlet UIImageView *wishlistimage;
@property (strong, nonatomic) IBOutlet UITextView *txtreview;
@property (strong, nonatomic) IBOutlet UITextField *txtreviewtitle;

@property (strong,nonatomic) UITextField *activeTextField;
@property(strong,nonatomic)UITextView *activeTextView;

@property (strong, nonatomic) IBOutlet StarRatingView *ratingView;
- (IBAction)didTapReview:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblreviewnumber;
@property (strong, nonatomic) IBOutlet UIToolbar *keyboardtoolbar;
- (IBAction)didTapDone:(id)sender;

@end
