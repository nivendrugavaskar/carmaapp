//
//  customlbl.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 05/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "customlbl.h"
#import "AppDelegate.h"

@implementation customlbl

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(UILabel *)setlabel:(NSString *)txt
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(85, 0, SCREEN_WIDTH-170, 44)];
    label.text=txt;
    label.tag=30;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Philosopher" size:20.0f];
   //label.minimumScaleFactor=0.5;
   // label.minimumFontSize=10.0;
    [label sizeThatFits:CGSizeMake(label.frame.size.width, label.frame.size.height)];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines=2;
    label.textColor =[UIColor whiteColor];
    return label;
}



@end
