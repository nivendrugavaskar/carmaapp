//
//  Custombarbutton.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 06/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Custombarbutton : UIBarButtonItem

+(UIButton *)setbarbutton;
+(UIButton *)setmenubarbutton;
+(UIButton *)setsearchbarbutton;
+(UIButton *)setcartbutton;
+(UIButton *)setaddtocartbutton:(NSString *)title;

@end
