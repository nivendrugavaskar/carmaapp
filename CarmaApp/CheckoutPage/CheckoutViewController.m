//
//  CheckoutViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 20/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "CheckoutViewController.h"
#import "AppDelegate.h"
#import "CheckoutTableViewCell.h"

@interface CheckoutViewController ()<connectionprotocol,UIAlertViewDelegate>
{
    int Qtyitem;
    NSMutableDictionary *Quantitydict,*pricedict;
    NSMutableArray *cartArray,*jsonarray;
    Connection *conn;
    AppDelegate *app;
    NSString *selectedservice;
    NSIndexPath *deletedindex;
    
    BOOL loadlist;
}

@end

@implementation CheckoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    
    
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
    
    
#pragma right menu bar
   
        UIButton *btn2=[Custombarbutton setaddtocartbutton:@"Continue"];
        [btn2 addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
        self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
    
    Qtyitem=1;
    Quantitydict=[[NSMutableDictionary alloc] init];
    pricedict=[[NSMutableDictionary alloc] init];
    
    
    loadlist=FALSE;
    

}
-(void)next
{
    if(cartArray.count==0)
    {
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Cart Empty" message:@"You have no items in cart" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
        return;
    }
    
#pragma new code
    jsonarray=[[NSMutableArray alloc] init];
    for(int i=0;i<cartArray.count;i++)
    {
        NSMutableDictionary *setdict=[[NSMutableDictionary alloc] init];
        setdict[@"quantity"] = [cartArray[i] valueForKey:@"quantity"];
        setdict[@"id_product"] = [cartArray[i] valueForKey:@"id_product"];
        [jsonarray addObject:setdict];
    }
    
    NSError *error;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:jsonarray options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:\n%@", jsonString);
    selectedservice=@"updatecart";
     NSDictionary *param=@{@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"],@"productdetails": jsonString};
    [conn servicecall:param :@"cartupdate.php"];
   // [self performSegueWithIdentifier:@"pushtoaddress" sender:self];
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Check Out"]];
    
     selectedservice=@"Fetchcart";
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    NSDictionary *param=@{@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"]};
    [conn servicecall:param :@"cartdetails.php"];
}
#pragma handle response
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"Fetchcart"])
    {
        app->totalproduct=[[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"total_product_in_cart"]] mutableCopy];
        [[NSUserDefaults standardUserDefaults] setValue:app->totalproduct forKey:@"cartbadge"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        cartArray=[[connectiondict valueForKey:@"result"] mutableCopy];
    }
    else if([selectedservice isEqualToString:@"deletecart"])
    {
        [cartArray removeObjectAtIndex:deletedindex.row];
        [self.tblView deleteRowsAtIndexPaths:@[deletedindex] withRowAnimation:UITableViewRowAnimationLeft];
        
        selectedservice=@"Fetchcart";
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        NSDictionary *param=@{@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"]};
        [conn servicecall:param :@"cartdetails.php"];
        
    }
    else if([selectedservice isEqualToString:@"updatecart"])
    {
//        selectedservice=@"Fetchcart";
//        conn=[[Connection alloc] init];
//        conn.connectionDelegate=self;
//        NSDictionary *param=[[NSDictionary alloc] initWithObjectsAndKeys:[app->logindetails valueForKeyPath:@"result.id_cart"],@"id_cart", nil];
//        [conn servicecall:param :@"cartdetails.php"];
        
        app->totalproduct=[[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"total_product_in_cart"]] mutableCopy];
        [[NSUserDefaults standardUserDefaults] setValue:app->totalproduct forKey:@"cartbadge"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSegueWithIdentifier:@"pushtoaddress" sender:self];
        
        
    }
    if(cartArray.count>0)
    {
        _no_cart_found.hidden=YES;
        
    }
    else
    {
        _no_cart_found.hidden=NO;
        
    }
    [self.tblView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return cartArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"Cell";
    
    CheckoutTableViewCell *cell = (CheckoutTableViewCell *)[self.tblView dequeueReusableCellWithIdentifier:cellNameFirst];
    
    
    if (cell == NULL)
    {
        cell = [[CheckoutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNameFirst];
    }
    
    
    cell.lblname.text=[NSString stringWithFormat:@"%@",[cartArray[indexPath.row] valueForKey:@"name"]];
    cell.lblprice.text=[NSString stringWithFormat:@"$%.2f",[[cartArray[indexPath.row] valueForKey:@"total"] floatValue]];
    cell.txtQtybox.text=[NSString stringWithFormat:@"%@",[cartArray[indexPath.row] valueForKey:@"quantity"]];
    
    //new code for image load
    NSString *imagepath=[NSString stringWithFormat:@"%@",[cartArray[indexPath.row] valueForKey:@"imagelink"]];
    //imagepath=[imagepath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor brownColor];
    activityIndicator.center = CGPointMake(CGRectGetMidX(cell.imgproduct.bounds), CGRectGetMidY(cell.imgproduct.bounds));
    activityIndicator.hidesWhenStopped = YES;
    
    __block BOOL successfetchimage=FALSE;
    __weak UIImageView *setimage = cell.imgproduct;
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
    [cell.imgproduct setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placeholderview.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         setimage.contentMode=UIViewContentModeScaleToFill;
         setimage.image=image;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
     }
     
                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator removeFromSuperview];
     }];
    
    
    if(!successfetchimage)
    {
        if(![imagepath isEqualToString:@""]||imagepath)
        {
            [cell.imgproduct addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }
    
    if([Quantitydict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
    {
        cell.txtQtybox.text=[Quantitydict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    }
    if([pricedict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
    {
        cell.lblprice.text=[pricedict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    }
    
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)didTapDelete:(id)sender {
    NSLog(@"DeleteClick");
    //CheckoutTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:@"Cell"];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    deletedindex=indexPath;
    
    UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Do you want to delete product from cart?" message:[cartArray[indexPath.row] valueForKeyPath:@"name"] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alrt.tag=100;
    [alrt show];
    
    
    
   
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
            deletedindex=[[NSIndexPath alloc] init];
        }
        else
        {
            selectedservice=@"deletecart";
            NSString *productid=[NSString stringWithFormat:@"%@",[cartArray[deletedindex.row] valueForKeyPath:@"id_product"]];
            NSDictionary *params=@{@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"],@"id_product": productid};
            [conn servicecall:params :@"cartdelete.php"];
        }
    }
}

- (IBAction)didTapPlus:(id)sender {
    
    CheckoutTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:@"Cell"];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
     NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    cell=(CheckoutTableViewCell *)[self.tblView cellForRowAtIndexPath:indexPath];
    Qtyitem=(int)(cell.txtQtybox.text).integerValue;
    Qtyitem++;
    cell.txtQtybox.text=[NSString stringWithFormat:@"%d",Qtyitem];
    
    float price=([[cartArray[indexPath.row] valueForKey:@"price"] floatValue])*Qtyitem;
    cell.lblprice.text=[NSString stringWithFormat:@"$%.2f",price];
    [Quantitydict setValue:cell.txtQtybox.text forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    [pricedict setValue:cell.lblprice.text forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    NSString *qty=[[cartArray[indexPath.row] valueForKey:@"quantity"] mutableCopy];
    qty=cell.txtQtybox.text;
    
    NSMutableDictionary *localdict=[cartArray[indexPath.row] mutableCopy];
    [localdict setValue:qty forKey:@"quantity"];
    
    cartArray[indexPath.row] = localdict;
    
    
}
- (IBAction)didTapminus:(id)sender
{
    
    CheckoutTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:@"Cell"];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    cell=(CheckoutTableViewCell *)[self.tblView cellForRowAtIndexPath:indexPath];
    Qtyitem=(int)(cell.txtQtybox.text).integerValue;
    if(Qtyitem>1)
    {
        Qtyitem--;
        cell.txtQtybox.text=[NSString stringWithFormat:@"%d",Qtyitem];
        float price=([[cartArray[indexPath.row] valueForKey:@"price"] floatValue])*Qtyitem;
        cell.lblprice.text=[NSString stringWithFormat:@"$%.2f",price];
        [Quantitydict setValue:cell.txtQtybox.text forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        [pricedict setValue:cell.lblprice.text forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        
        NSString *qty=[[cartArray[indexPath.row] valueForKey:@"quantity"] mutableCopy];
        qty=cell.txtQtybox.text;
        
        NSMutableDictionary *localdict=[cartArray[indexPath.row] mutableCopy];
        [localdict setValue:qty forKey:@"quantity"];
        
        cartArray[indexPath.row] = localdict;
    }
}

@end
