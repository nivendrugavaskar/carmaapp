//
//  CheckoutTableViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 20/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckoutTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *txtQtybox;
@property (strong, nonatomic) IBOutlet UIImageView *imgproduct;
@property (strong, nonatomic) IBOutlet UILabel *lblprice;
@property (strong, nonatomic) IBOutlet UILabel *lblname;

@end
