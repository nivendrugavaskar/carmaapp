//
//  CheckoutViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 20/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckoutViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UILabel *no_cart_found;

@end
