//
//  CheckoutTableViewCell.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 20/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "CheckoutTableViewCell.h"

@implementation CheckoutTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
