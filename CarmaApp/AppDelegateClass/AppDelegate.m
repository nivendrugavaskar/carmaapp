//
//  AppDelegate.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 05/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "AppDelegate.h"
@interface AppDelegate ()
{
    
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedindetails"];
    logindetails = [[NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData] mutableCopy];
    totalproduct=[[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"cartbadge"] mutableCopy];
    
    [NSThread sleepForTimeInterval:5.0];
    
    // hi
#pragma new code for pinterest
    _pinterest=[[Pinterest alloc] initWithClientId:@"4792917272358289587"];
    
#pragma new code twitter
   //[PDKClient configureSharedInstanceWithAppId:@"1447189"];
    //./Fabric.framework/run a38af44f048fa6c6426d97638df0ab9a19700a18 876c92c84231dad70ce2433bf4317eda8d5de39cc875df00536d6b3427ad75ad
   
  //  [[Twitter sharedInstance] startWithConsumerKey:@"4AG1p44sD4Ijze16vGAq6NWhp" consumerSecret:@"fO34rIU4HTael1hhjsG3mBvgT8X0uIRmRoZGlcYnKFstQIG4Ox"];
//    [Fabric with:@[[Twitter sharedInstance]]];
    if(IS_OS_8_OR_LATER)
    {
    [Fabric with:@[[Twitter sharedInstance]]];
     [Fabric with:@[[Twitter class]]];
    }
#pragma parent url
     parentUrl=@"http://dev.businessprodemo.com/Carmaliving/php2/appwebservice/";
    // parentUrl=@"http://test2.businessprodemo.com/Carmaliving/v1/appwebservice/";
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
    UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuViewController"];
    //UIViewController *rightSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"rightSideMenuViewController"];
    container.panMode = MFSideMenuPanModeNone;
    container.leftMenuViewController = leftSideMenuViewController;
    //[container setRightMenuViewController:rightSideMenuViewController];
    container.centerViewController = navigationController;
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
   
    if([sourceApplication rangeOfString:@"facebook"].location!=NSNotFound)
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                        annotation:annotation];
    }
    else
    {
        return [GPPURLHandler handleURL:url sourceApplication:sourceApplication
                             annotation:annotation];
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
