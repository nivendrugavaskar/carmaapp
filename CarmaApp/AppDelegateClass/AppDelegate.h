//
//  AppDelegate.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 05/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "customlbl.h"
#import "CustomnavigationController.h"
#import "Custombarbutton.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#pragma facebook api import
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
//#import <FacebookSDK/FacebookSDK.h>
#pragma svprogresshud
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"
#pragma global service
#import "Connection.h"
#pragma validation Category
#import "NSString+Validation.h"
#pragma hide cursor
#import "Hidecursor.h"
#import "SingleToneClass.h"
#import <Accounts/Accounts.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

#import <Pinterest/Pinterest.h>


 #import "AsyncImageView.h" 



#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

static NSString * const kClientId = @"759096274763-50rvojkti9jflq3526tsrtom25p4heoe.apps.googleusercontent.com";

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
@public
    NSString *parentUrl,*totalproduct;
    NSMutableDictionary *logindetails;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong,nonatomic)Pinterest *pinterest;


@end

