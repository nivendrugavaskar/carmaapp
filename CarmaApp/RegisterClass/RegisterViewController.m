//
//  RegisterViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 06/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "RegisterViewController.h"
#import "AppDelegate.h"

@interface RegisterViewController ()<connectionprotocol>
{
    AppDelegate *app;
    Connection *conn;
    NSMutableDictionary *regDict;
    
    CGPoint pointtrack,pointtrackcontentoffset;
}

@end

@implementation RegisterViewController

@synthesize keyBoardToolBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // new code
    [self setbarbuttons];
#pragma set fame to manage animation
    pointtrackcontentoffset=self.view.frame.origin;
    
    for (id v in self.view.subviews) {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *innerview=(UIView *)v;
            for (id v in innerview.subviews)
            {
                if([v isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfld=(UITextField *)v;
                    txtfld.inputAccessoryView=keyBoardToolBar;
                }
            }
        }
    }
    
    
}
#pragma set barbuttons
-(void)setbarbuttons
{
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItems = @[leftbarbutton];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Sign Up"]];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapRegister:(id)sender
{
    [self dismisskeyboard];
    for (id v in self.view.subviews) {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *innerview=(UIView *)v;
            for (id v in innerview.subviews)
            {
                if([v isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfld=(UITextField *)v;
                    if(![NSString validation:txtfld.text])
                    {
                        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"All fields are mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        return;
                    }
                    else if(txtfld.tag==3||txtfld.tag==4)
                    {
                        if(![NSString validateEmail:txtfld.text])
                        {
                            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Incorrect" message:@"Please enter correct email address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            return;
                        }
                    }
                    else if(![_txtEmail.text isEqualToString:_txtConfirmEmail.text])
                    {
                        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Incorrect" message:@"Please enter correct confirm email address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        _txtConfirmEmail.text=@"";
                        return;
                    }
                    else if(![_txtPassword.text isEqualToString:_txtConfirmPassword.text])
                    {
                        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Incorrect" message:@"Please enter correct confirm reenter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        _txtConfirmPassword.text=@"";
                        return;
                    }
                    
                }
            }
            
        }
    }
    
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    //email,firstname,lastname,passwd
    NSDictionary *params=@{@"email": _txtEmail.text,@"firstname": _txtFirstName.text,@"lastname": _txtLastName.text,@"passwd": _txtPassword.text};
    
    [conn servicecall:params :@"customer_registration.php"];
    
}
#pragma textfield delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack = textField.superview.frame.origin;
    
    CGRect rect=self.view.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(SCREEN_HEIGHT==480||SCREEN_HEIGHT==568)
    {
        if(pointtrack.y>120)
        {
            if(pointtrack.y>350)
            {
                pointtrack.y=340;
            }
            rect.origin.y-=pointtrack.y-130;
        }
    }
    
    else if(SCREEN_HEIGHT==667)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-100;
        }
    }
    else if(SCREEN_HEIGHT==736)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-120;
        };
    }
    else
    {
        
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
    return NO;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(!(self.activeTextField.tag==1||self.activeTextField.tag==2))
    {
        if([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    return YES;
}

#pragma dismiss keyboard
-(void)dismisskeyboard
{
    [self.activeTextField resignFirstResponder];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
}

-(void)sucessdone:(NSDictionary *)connectiondict
{
    if(!connectiondict)
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        return;
    }
    else
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        regDict=[[NSMutableDictionary alloc] initWithDictionary:connectiondict];
#pragma updating details
        app->logindetails=[[NSMutableDictionary alloc] initWithDictionary:regDict];
         app->totalproduct=[NSString stringWithFormat:@"%@",[regDict valueForKeyPath:@"result.total_product_in_cart"]];
#pragma save user details
        NSString *useremail=[regDict valueForKeyPath:@"result.email"];
        NSString *pwd=_txtPassword.text;
        [[NSUserDefaults standardUserDefaults] setValue:useremail forKey:@"useremail"];
        [[NSUserDefaults standardUserDefaults] setValue:pwd forKey:@"pwd"];
        [[NSUserDefaults standardUserDefaults] setValue:@"emaillogin" forKey:@"LoginType"];
        // new code to save dict
#pragma save dictionary
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:app->logindetails];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"loggedindetails"];
        [[NSUserDefaults standardUserDefaults] setObject:app->totalproduct forKey:@"cartbadge"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSegueWithIdentifier:@"pushtocategory" sender:self];
        
    }
    
}
- (IBAction)didTapnext:(id)sender {
    NSInteger nextTag = self.activeTextField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapPrevious:(id)sender {
    
    NSInteger nextTag = self.activeTextField.tag - 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapDone:(id)sender {
    [self dismisskeyboard];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
