//
//  CategoryViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 07/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "CategoryViewController.h"
#import "AppDelegate.h"
#import "CategoryTableViewCell.h"
#import "ProductViewController.h"

@interface CategoryViewController ()<connectionprotocol>
{
    Connection *conn;
    NSMutableDictionary *categoryDict;
    NSMutableArray *categoryArray;
    
    BOOL loadpage;
}

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     //[self setNeedsStatusBarAppearanceUpdate];
    
    [self setbarbuttons];
    loadpage=FALSE;
}
//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}
#pragma response delegate
-(void)sucessdone:(NSDictionary *)connectiondict
{
    if(!connectiondict)
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        return;
    }
    else
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        categoryDict=[[NSMutableDictionary alloc] initWithDictionary:connectiondict];
        categoryArray=[[NSMutableArray alloc] init];
        categoryArray=[categoryDict valueForKeyPath:@"result"];
        [self.tblView reloadData];
        
    }
}

#pragma set barbuttons
-(void)setbarbuttons
{
        self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;

#pragma left menu bar
        UIButton *btn1=[Custombarbutton setmenubarbutton];
        [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn1];
        self.navigationItem.leftBarButtonItems = @[leftbarbutton];
    
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setsearchbarbutton];
    [btn2 addTarget:self action:@selector(searchbutton) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)searchbutton
{
    [self performSegueWithIdentifier:@"pushtosearch" sender:self];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Category"]];
    
    if(!loadpage)
    {
        NSLog(@"Service call");
#pragma category list service
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        [conn servicecall:nil :@"categorylist.php"];
        loadpage=TRUE;
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return categoryArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(SCREEN_HEIGHT==480||SCREEN_HEIGHT==568)
    {
    return 102;
    }
    else if(SCREEN_HEIGHT==667)
    {
    return 116;
    }
    else if (SCREEN_HEIGHT==736)
    {
    return 126;
    }
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"Cellimg";
    
    CategoryTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:cellNameFirst forIndexPath:indexPath];
    cell.categorynametxt.text=[categoryArray[indexPath.row] valueForKey:@"name"];
    
    //new code for image load
    NSString *imagepath=[NSString stringWithFormat:@"%@",[categoryArray[indexPath.row] valueForKey:@"image"]];
    //imagepath=[imagepath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor brownColor];
    activityIndicator.center = CGPointMake(CGRectGetMidX(cell.imgViewCategory.bounds), CGRectGetMidY(cell.imgViewCategory.bounds));
    activityIndicator.hidesWhenStopped = YES;
    
    __block BOOL successfetchimage=FALSE;
    __weak UIImageView *setimage = cell.imgViewCategory;
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
    [cell.imgViewCategory setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"noimage.jpg"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         setimage.contentMode=UIViewContentModeScaleToFill;
         setimage.image=image;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
     }
    
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator removeFromSuperview];
     }];
    
    
    if(!successfetchimage)
    {
        if(![imagepath isEqualToString:@""]||imagepath)
        {
            [cell.imgViewCategory addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // NSLog(@"last index path=%ld",(long)lastindexpath.row);
     [self performSegueWithIdentifier:@"pustToproduct" sender:indexPath];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pustToproduct"])
    {
        NSIndexPath *indexpath=(NSIndexPath *)sender;
        
        ProductViewController *inst=(ProductViewController *)segue.destinationViewController;
        inst.categoryArray=categoryArray;
        inst.selectedcatg=[categoryArray[indexpath.row] valueForKey:@"name"];
        inst.selectedcategorydict=categoryArray[indexpath.row];
    }
}


@end
