//
//  CategoryViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 07/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tblView;

@end
