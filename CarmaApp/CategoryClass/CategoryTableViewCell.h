//
//  CategoryTableViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 07/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CategoryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *categorynametxt;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCategory;

@end
