//
//  ShippingAddressViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 10/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ShippingAddressViewController.h"
#import "AppDelegate.h"
#import "ShippingAddressTableViewCell.h"
#import "EditAddresViewController.h"

@interface ShippingAddressViewController ()<UITableViewDelegate,UITableViewDataSource,connectionprotocol,listingaddressprotocal>
{
    AppDelegate *app;
    NSMutableArray *shippingaddressarray;
    NSMutableDictionary *selecteddict;
    Connection *conn;
    NSString *selectedservice;
    
    BOOL fromnextpage;
}

@end

@implementation ShippingAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setaddtocartbutton:@"Continue"];
    [btn2 addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
    selecteddict=[[NSMutableDictionary alloc] init];
    fromnextpage=FALSE;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Choose Your Delivery Address"]];
    
    
    
    if(!fromnextpage)
    {
        
#pragma fetching user address
        selectedservice=@"Fetchaddress";
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        NSString *customerid=[NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]];
        NSDictionary *params=@{@"id_customer": customerid};
        [conn servicecall:params :@"addresslist.php"];
    }
    
}

#pragma response delegate
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"Fetchaddress"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
            shippingaddressarray=[[NSMutableArray alloc] init];
            shippingaddressarray=[[connectiondict valueForKey:@"result"] mutableCopy];
            if(shippingaddressarray.count==0)
            {
                
                [self performSegueWithIdentifier:@"pushtoeditaddress" sender:self];
            }
            else
            {
                [self.tblView reloadData];
            }
        }
    }
    
}
#pragma alert view delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}
#pragma update table view from edit page
- (void)buttonTappedsave
{
    fromnextpage=FALSE;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

-(void)next
{
    if(selecteddict.allKeys.count==0)
    {
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Required" message:@"please select any address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
        return;
    }
   [self performSegueWithIdentifier:@"pushtoshipping" sender:self];
    
}

-(void)back
{
    
[self.navigationController popViewControllerAnimated:YES];
    
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoeditaddress"])
    {
        EditAddresViewController *inst=(EditAddresViewController *)segue.destinationViewController;
        if([sender isKindOfClass:[NSIndexPath class]])
        {
            NSIndexPath *indexpath=(NSIndexPath *)sender;
            inst.addressDict=shippingaddressarray[indexpath.row];
            inst.selectedaddress=@"update";
            inst.checkaddressarraycount=(int)shippingaddressarray.count;
        }
        else
        {
            inst.checkaddressarraycount=(int)shippingaddressarray.count;
            inst.selectedaddress=@"Add";
        }
        
        inst.editdelegate=self;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return shippingaddressarray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[shippingaddressarray[indexPath.row] valueForKey:@"company"] isEqualToString:@""])
    {
        return 179;
    }
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"Cell";
    ShippingAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellNameFirst];
    if (cell == NULL)
    {
        cell = [[ShippingAddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNameFirst];
    }
    
    if([selecteddict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
    {
        cell.imgradiobtn.image=[UIImage imageNamed:@"radio_btn3.png"];
    }
    else
    {
        cell.imgradiobtn.image=[UIImage imageNamed:@"radio_btn2.png"];
    }
    
#pragma condition maintain
    if([[shippingaddressarray[indexPath.row] valueForKey:@"company"] isKindOfClass:[NSNull class]]||[[shippingaddressarray[indexPath.row] valueForKey:@"company"] isEqualToString:@""])
    {
#pragma design maintain
        for (id v in cell.contentView.subviews)
        {
            if([v isKindOfClass:[UIView class]])
            {
                UIView *vw=(UIView *)v;
                
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UILabel class]])
                    {
                        UILabel *lbl=(UILabel *)v;
                        if(lbl.tag>2)
                        {
                            if(lbl.tag==3)
                            {
                                lbl.hidden=YES;
                            }
                            else
                            {
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-2)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                        }
                        
                    }
                }
            }
        }
    }
    else
    {
        for (id v in cell.contentView.subviews)
        {
            if([v isKindOfClass:[UIView class]])
            {
                UIView *vw=(UIView *)v;
                
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UILabel class]])
                    {
                        UILabel *lbl=(UILabel *)v;
                        if(lbl.tag>2)
                        {
                            if(lbl.tag==3)
                            {
                                lbl.hidden=NO;
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-1)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                            else
                            {
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-1)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                        }
                    }
                }
            }
        }
    }
    
    cell.lbladdresstitle.text=[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"alias"]];
    cell.lbladdress.text=[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"address1"]];
    cell.lblzip.text=[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"postcode"]];
    cell.lblcity.text=[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"city"]];
    cell.lblcountry.text=[NSString stringWithFormat:@"%@, %@",[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"country_name"]],[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"state_name"]]];
#pragma trimming space
    NSString *firstname=[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"firstname"]];
    firstname=[firstname stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *lastname=[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"lastname"]];
    lastname=[lastname stringByReplacingOccurrencesOfString:@" " withString:@""];
    cell.lblname.text=[NSString stringWithFormat:@"%@ %@",firstname,lastname];
    cell.lblcompanyname.text=[NSString stringWithFormat:@"%@",[shippingaddressarray[indexPath.row] valueForKey:@"company"]];
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ShippingAddressTableViewCell *cell=(ShippingAddressTableViewCell *)[self.tblView cellForRowAtIndexPath:indexPath];
    cell.imgradiobtn.image=[UIImage imageNamed:@"radio_btn3.png"];
    [selecteddict setValue:@"select" forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShippingAddressTableViewCell *cell=(ShippingAddressTableViewCell *)[self.tblView cellForRowAtIndexPath:indexPath];
    cell.imgradiobtn.image=[UIImage imageNamed:@"radio_btn2.png"];
    [selecteddict setValue:nil forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
}
- (IBAction)didTapUpdate:(id)sender {
    
    fromnextpage=TRUE;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    [self performSegueWithIdentifier:@"pushtoeditaddress" sender:indexPath];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
