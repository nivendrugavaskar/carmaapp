//
//  ShippingAddressViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 10/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingAddressViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tblView;

@end
