//
//  EditAddresViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 01/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol listingaddressprotocal
- (void)buttonTappedsave;
@end

@interface EditAddresViewController : UIViewController<UITextFieldDelegate>


@property int checkaddressarraycount;
@property NSMutableDictionary *addressDict;

@property NSString *selectedaddress;

@property UITextField *activeTextField;
@property (strong, nonatomic) IBOutlet UIToolbar *keyBoardToolbar;
@property (strong, nonatomic) IBOutlet UIScrollView *aScrollview;
@property (strong, nonatomic) IBOutlet UITextField *txtFirstname;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtPostal;
@property (strong, nonatomic) IBOutlet UITextField *txtCity;
@property (strong, nonatomic) IBOutlet UITextField *txtCountry;
@property (strong, nonatomic) IBOutlet UITextField *txtState;
@property (strong, nonatomic) IBOutlet UITextField *txtHomePhone;
@property (strong, nonatomic) IBOutlet UITextField *txtMobilePhone;
@property (strong, nonatomic) IBOutlet UITextField *txtAddressTitle;
@property (strong, nonatomic) IBOutlet UITextField *txtcompanyname;


@property (assign) id <listingaddressprotocal> editdelegate;

@end
