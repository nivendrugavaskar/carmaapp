//
//  EditAddresViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 01/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "EditAddresViewController.h"
#import "AppDelegate.h"


@interface EditAddresViewController ()<connectionprotocol,UIPickerViewDataSource,UIPickerViewDelegate,UIAlertViewDelegate>
{
    CGPoint pointtrack,pointtrackcontentoffset;
    CGSize kbsize,contentsize;
    AppDelegate *app;
    NSMutableArray *countryname,*countryid,*statename,*stateid;
    NSString *selectedcountryid,*selectedstateid,*selectedservice,*previouscountryid;
    Connection *conn;
    UIPickerView *countrypicker,*statepicker;
    int countryindex,stateindex;
    
    __block UIActivityIndicatorView *activityIndicator;
    
}

@end

@implementation EditAddresViewController

@synthesize keyBoardToolbar,aScrollview;

- (void)viewDidLoad {
    [super viewDidLoad];
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view.
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    if(self.checkaddressarraycount==0)
    {
     self.navigationItem.leftBarButtonItems = @[leftbarbutton1];   
    }
    else
    {
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
    }
#pragma right menu bar
    
    UIButton *btn2=[Custombarbutton setaddtocartbutton:@"Save"];
    [btn2 addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
    [self registerForKeyboardNotifications];
    pointtrackcontentoffset=self.aScrollview.contentOffset;
#pragma set toolbar
    for (id v in self.aScrollview.subviews) {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *innerview=(UIView *)v;
            for (id v in innerview.subviews)
            {
                if([v isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfld=(UITextField *)v;
                    txtfld.inputAccessoryView=keyBoardToolbar;
                }
            }
        }
    }
    
    selectedservice=@"country";
    
#pragma initialize country and state
    
    countryname=[[NSMutableArray alloc] init];
    countryid=[[NSMutableArray alloc] init];
    statename=[[NSMutableArray alloc] init];
    stateid=[[NSMutableArray alloc] init];
    selectedcountryid=[[NSString alloc] init];
    previouscountryid=[[NSString alloc] init];
    selectedstateid=[[NSString alloc] init];
    
    _txtState.text=@"-";
    
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    [conn servicecall:nil :@"countrylist.php"];
    
#pragma set contentsize
    pointtrackcontentoffset=self.aScrollview.contentOffset;
    contentsize.width=0;
    contentsize.height=self.txtAddressTitle.superview.frame.origin.y+self.txtAddressTitle.superview.frame.size.height+20;
    aScrollview.contentSize = contentsize;
    
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = aNotification.userInfo;
    kbsize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    aScrollview.contentSize = contentsize;
    aScrollview.contentOffset = pointtrackcontentoffset;
    aScrollview.contentSize = CGSizeMake(aScrollview.contentSize.width,self.txtAddressTitle.superview.frame.origin.y+self.txtAddressTitle.superview.frame.size.height+20+kbsize.height);
    
}



#pragma load filled data
-(void)loaddata
{
 
    _txtFirstname.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"firstname"]];
    _txtLastName.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"lastname"]];
    _txtAddress.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"address1"]];
    _txtPostal.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"postcode"]];
    _txtCity.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"city"]];
    _txtCountry.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"country_name"]];
    _txtState.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"state_name"]];
    _txtHomePhone.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"phone"]];
    _txtMobilePhone.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"phone_mobile"]];
    _txtAddressTitle.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"alias"]];
    if([[self.addressDict valueForKey:@"company"] isKindOfClass:[NSNull class]])
    {
      _txtcompanyname.text=@"";
    }
    else
    {
      _txtcompanyname.text=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"company"]];
    }
    
#pragma track id country and state
    selectedcountryid=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"id_country"]];
    selectedstateid=[NSString stringWithFormat:@"%@",[self.addressDict valueForKey:@"id_state"]];
    previouscountryid=selectedcountryid;
   
    
}
#pragma response delegate
-(void)sucessdone:(NSDictionary *)connectiondict
{
    if([selectedservice isEqualToString:@"country"])
    {
        if(!connectiondict)
        {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
            if(activityIndicator)
            {
            [activityIndicator removeFromSuperview];
            }
            return;
        }
        else
        {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
            countryname=[connectiondict valueForKey:@"result"];
            selectedcountryid=[NSString stringWithFormat:@"%@",[countryname[0] valueForKey:@"countryid"]];
            _txtCountry.text=[countryname[0] valueForKey:@"countryname"];
            [activityIndicator removeFromSuperview];
            [countrypicker reloadAllComponents];
            
#pragma new code to load data
            if([self.selectedaddress isEqualToString:@"update"])
            {
                NSLog(@"fetched data:-%@",self.addressDict);
                [self loaddata];
            }
   
        }
    }
    else if([selectedservice isEqualToString:@"state"])
    {
        if(!connectiondict)
        {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
            [activityIndicator removeFromSuperview];
            return;
        }
        else
        {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
            statename=[connectiondict valueForKey:@"result"];
            [activityIndicator removeFromSuperview];
            [statepicker reloadAllComponents];
            
        }
    }
    else if([selectedservice isEqualToString:@"addaddress"])
    {
        if(!connectiondict)
        {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
            return;
        }
        else
        {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Success" message:[connectiondict valueForKeyPath:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=100;
            [alert show];
        }
        
    }
    else if([selectedservice isEqualToString:@"updateaddress"])
    {
        if(!connectiondict)
        {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
            return;
        }
        else
        {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Success" message:[connectiondict valueForKeyPath:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=100;
            [alert show];
        }
        
    }
}
#pragma alert view delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
      [self.editdelegate buttonTappedsave];
      [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Address"]];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

-(void)next
{
    [self dismisskeyboard];
    for (id v in self.aScrollview.subviews)
    {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *innerview=(UIView *)v;
            for (id v in innerview.subviews)
            {
                if([v isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfld=(UITextField *)v;
                    if(txtfld.tag!=3)
                    {
                    if(![NSString validation:txtfld.text])
                    {
                        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"All fields are mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        return;
                    }
                    }
                    else if([_txtState.text isEqualToString:@"-"])
                    {
                        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please select state" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        return;
                    }
                
                }
            }
            
        }
    }
    if([self.selectedaddress isEqualToString:@"update"])
    {
        selectedservice=@"updateaddress";
        // firstname,lastname,address1,postcode,city,id_country,id_state,phone,phone_mobile,alias,id_customer
        NSDictionary *params=@{@"firstname": _txtFirstname.text,@"lastname": _txtLastName.text,@"address1": _txtAddress.text,@"postcode": _txtPostal.text,@"city": _txtCity.text,@"id_country": selectedcountryid,@"id_state": selectedstateid,@"phone": _txtHomePhone.text,@"phone_mobile": _txtMobilePhone.text,@"alias": _txtAddressTitle.text,@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]],@"id_address": [self.addressDict valueForKey:@"id_address"],@"company": _txtcompanyname.text};
        
        [conn servicecall:params :@"updateaddress.php"];
    }
    else if([self.selectedaddress isEqualToString:@"Add"])
    {
        selectedservice=@"addaddress";
        // firstname,lastname,address1,postcode,city,id_country,id_state,phone,phone_mobile,alias,id_customer
        NSDictionary *params=@{@"firstname": _txtFirstname.text,@"lastname": _txtLastName.text,@"address1": _txtAddress.text,@"postcode": _txtPostal.text,@"city": _txtCity.text,@"id_country": selectedcountryid,@"id_state": selectedstateid,@"phone": _txtHomePhone.text,@"phone_mobile": _txtMobilePhone.text,@"alias": _txtAddressTitle.text,@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]],@"company": _txtcompanyname.text};
        
        [conn servicecall:params :@"addaddress.php"];
    }
    
    
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma textfield delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack = textField.superview.frame.origin;
    pointtrackcontentoffset=self.aScrollview.contentOffset;
    
    
    if(self.activeTextField==_txtCountry)
    {
        countrypicker=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 250)];
        countrypicker.dataSource = self;
        countrypicker.delegate = self;
        countrypicker.tag=1001;
        [countrypicker setShowsSelectionIndicator:YES];
        _txtCountry.inputView = countrypicker;
        
        
        
        if(countryname.count==0)
        {
            selectedservice=@"country";
            // [self countryandstatefetchservice];
            activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            activityIndicator.color=[UIColor brownColor];
            activityIndicator.center = CGPointMake(CGRectGetMidX(statepicker.bounds), CGRectGetMidY(statepicker.bounds));
            activityIndicator.hidesWhenStopped = YES;
            [statepicker addSubview:activityIndicator];
            [activityIndicator startAnimating];
            
            conn=[[Connection alloc] init];
            conn.connectionDelegate=self;
            [conn servicecall:nil :@"countrylist.php"];
        }
        else
        {
            selectedservice=@"country";
            [countrypicker reloadAllComponents];
        }
    }
    
    else if(self.activeTextField==_txtState)
    {
        statepicker=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 250)];
        statepicker.dataSource = self;
        statepicker.delegate = self;
        statepicker.tag=1002;
        [statepicker setShowsSelectionIndicator:YES];
        _txtState.inputView = statepicker;
        
        // new code
        if(![previouscountryid isEqualToString:selectedcountryid]||statename.count==0)
        {
            activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            activityIndicator.color=[UIColor brownColor];
            activityIndicator.center = CGPointMake(CGRectGetMidX(statepicker.bounds), CGRectGetMidY(statepicker.bounds));
            activityIndicator.hidesWhenStopped = YES;
            [statepicker addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
        if([selectedcountryid isEqualToString:@""])
        {
            selectedservice=@"state";
            statename=[[NSMutableArray alloc] init];
            [statename addObject:@"Select country first"];
            [statepicker reloadAllComponents];
        }
        else
        {
            // new code
            if(![previouscountryid isEqualToString:selectedcountryid])
            {
                selectedservice=@"state";
                NSDictionary *params=@{@"countryid": selectedcountryid};
                [conn servicecall:params :@"statelist.php"];
            }
            else if(statename.count==0)
            {
                selectedservice=@"state";
                NSDictionary *params=@{@"countryid": selectedcountryid};
                [conn servicecall:params :@"statelist.php"];
            }
            else
            {
                [statepicker reloadAllComponents];
            }
        }
    }
    
    if(SCREEN_HEIGHT==480)
    {
        if (pointtrack.y>50)
        {
            
                if(pointtrack.y>420)
                {
                    pointtrack.y=440;
                }
                [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-50) animated:YES];
            
        }
        else
        {
            [self.aScrollview setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        pointtrackcontentoffset=CGPointMake(aScrollview.contentOffset.x, pointtrack.y-50);
        
    }
   
    else if(SCREEN_HEIGHT==568)
    {
        if (pointtrack.y>80)
        {
            
                if(pointtrack.y>370)
                {
                    pointtrack.y=390;
                }
                 [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-100) animated:YES];
            
        }
        else
        {
            [self.aScrollview setContentOffset:CGPointMake(0, 0) animated:YES];
        }
         pointtrackcontentoffset=CGPointMake(aScrollview.contentOffset.x, pointtrack.y-100);
        
    }
    
    else if(SCREEN_HEIGHT==667)
    {
        if(pointtrack.y>250)
        {
            [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-220) animated:YES];
        }
        else
        {
            [self.aScrollview setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        pointtrackcontentoffset=self.aScrollview.contentOffset;
    }
    else if(SCREEN_HEIGHT==736)
    {
        if(pointtrack.y>320.0)
        {
          [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-280) animated:YES];
        }
        else
        {
            [self.aScrollview setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        pointtrackcontentoffset=CGPointMake(aScrollview.contentOffset.x, pointtrack.y-280);
    }
    else
    {
        
    }
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
    return NO;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
     NSUInteger newLength = (textField.text).length + string.length - range.length;
    
    if((self.activeTextField.tag==7||self.activeTextField.tag==8))
    {
            return NO;
    }
    else if((self.activeTextField.tag==9||self.activeTextField.tag==10))
    {
        if([string isEqualToString:@" "])
        {
        return NO;
        }
    }
    else if(self.activeTextField==_txtPostal)
    {
        BOOL correctvalue=[NSString validation3:textField.text :range :string];
        if(!correctvalue)
        {
            return correctvalue;
        }
        else
        {
            if(newLength>4)
            {
                return NO;
            }
        }
    }
    
    return YES;
}

#pragma dismiss keyboard
-(void)dismisskeyboard
{
    [self.activeTextField resignFirstResponder];
    if(self.activeTextField==_txtCountry||self.activeTextField==_txtState)
    {
        [self PickerSelection];
    }
    
    (self.aScrollview).contentSize = contentsize;
    [self.aScrollview setContentOffset:CGPointMake(0, 0) animated:YES];
    
}
- (IBAction)didTapnext:(id)sender {
    NSInteger nextTag = self.activeTextField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapPrevious:(id)sender {
    
    NSInteger nextTag = self.activeTextField.tag - 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapDone:(id)sender
{
    [self dismisskeyboard];
}
-(void)PickerSelection
{
    if(self.activeTextField==_txtCountry)
    {
        if(countryname.count>0)
        {
            countryindex = (int)[countrypicker selectedRowInComponent:0];
            _txtCountry.text=[countryname[countryindex] valueForKey:@"countryname"];
            selectedcountryid=[NSString stringWithFormat:@"%@",[countryname[countryindex] valueForKey:@"countryid"]];
            if(![previouscountryid isEqualToString:selectedcountryid])
            {
            selectedstateid=@"";
            _txtState.text=@"-";
            }
        }
        else
        {
            [self dismisskeyboard];
        }
    }
    else
    {
        if(statename.count>0)
        {
            stateindex = (int)[statepicker selectedRowInComponent:0];
            if([selectedcountryid isEqualToString:@""])
            {
                _txtState.text=@"-";
            }
            else
            {
                // new code
                _txtState.text=[statename[stateindex] valueForKey:@"statename"];
                selectedstateid=[NSString stringWithFormat:@"%@",[statename[stateindex] valueForKey:@"stateid"]];
                
                // new code
                previouscountryid=selectedcountryid;
            }
            
        }
        else
        {
            [self dismisskeyboard];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark Picker Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerView.tag==1001)
    {
        return [NSString stringWithFormat:@"%@", [countryname[row] valueForKey:@"countryname"]];
    }
    if([selectedcountryid isEqualToString:@""])
    {
        return [NSString stringWithFormat:@"%@", statename[row]];
    }
    
    return [NSString stringWithFormat:@"%@", [statename[row] valueForKey:@"statename"]];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if(pickerView.tag==1001)
    {
        return countryname.count;
    }
    
    return statename.count;
}
@end
