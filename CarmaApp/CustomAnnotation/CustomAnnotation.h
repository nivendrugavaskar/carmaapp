//
//  CustomAnnotation.h
//  Deuces
//
//  Created by GauravKumar on 18/08/15.
//  Copyright (c) 2015 Rakesh Jyoti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomAnnotation : NSObject  <MKAnnotation> {
    
}

@property(nonatomic, assign) CLLocationCoordinate2D coordinate;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *saveIndex;
@property int tag;

@end
