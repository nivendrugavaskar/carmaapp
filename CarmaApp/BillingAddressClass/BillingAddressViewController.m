//
//  AddressViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 25/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "BillingAddressViewController.h"
#import "AppDelegate.h"
#import "BillingAddressTableViewCell.h"
#import "EditAddresViewController.h"

@interface BillingAddressViewController ()<UITableViewDataSource,UITableViewDelegate,connectionprotocol,listingaddressprotocal>
{
    BOOL ischecked;
    NSMutableArray *addressarray;
    NSMutableDictionary *selecteddict;
    NSString *selectedservice;
    BOOL fromnextpage;
    Connection *conn;
    AppDelegate *app;
}

@end

@implementation BillingAddressViewController

@synthesize billingdict;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setaddtocartbutton:@"Continue"];
    [btn2 addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
    selecteddict=[[NSMutableDictionary alloc] init];
#pragma set default parameter and other stuff
    ischecked=FALSE;
    fromnextpage=FALSE;
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Choose Your Billing Address"]];
    
    if(!fromnextpage)
    {
#pragma fetching user address
        selectedservice=@"Fetchaddress";
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        NSString *customerid=[NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]];
        NSDictionary *params=@{@"id_customer": customerid};
        [conn servicecall:params :@"addresslist.php"];
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
//    if([addressarray count]==0)
//    {
//        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//        [SVProgressHUD dismiss];
//       [self performSegueWithIdentifier:@"pushtoeditaddress" sender:self];
//    }
}

#pragma response delegate
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"Fetchaddress"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
            addressarray=[[NSMutableArray alloc] init];
            addressarray=[[connectiondict valueForKey:@"result"] mutableCopy];
            if(addressarray.count==0)
            {
                _billingaddressview.hidden=YES;
                UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"No Address Found" message:@"Please add an address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alrt.tag=101;
                [alrt show];
                
                
            }
            else
            {
                _billingaddressview.hidden=NO;
                
                [self.tblView reloadData];
            }
        }
    }
    
}

#pragma alert view delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==101)
    {
       [self performSegueWithIdentifier:@"pushtoeditaddress" sender:self];
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

-(void)next
{
    if(selecteddict.allKeys.count==0)
    {
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Required" message:@"please select any address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
        return;
    }
    if(ischecked)
    {
    [[SingleToneClass sharedManager] setShippinginfoInfoDict:billingdict];
    [self performSegueWithIdentifier:@"pushtoshipping" sender:self];
    }
    else
    {
    [self performSegueWithIdentifier:@"pushtodelivery" sender:self];
    }
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
#pragma update table view from edit page
- (void)buttonTappedsave
{
    fromnextpage=FALSE;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoeditaddress"])
    {
        EditAddresViewController *inst=(EditAddresViewController *)segue.destinationViewController;
        if([sender isKindOfClass:[NSIndexPath class]])
        {
            NSIndexPath *indexpath=(NSIndexPath *)sender;
            inst.addressDict=addressarray[indexpath.row];
            inst.selectedaddress=@"update";
            inst.checkaddressarraycount=(int)addressarray.count;
        }
        else
        {
            inst.checkaddressarraycount=(int)addressarray.count;
            inst.selectedaddress=@"Add";
        }
        
        inst.editdelegate=self;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return addressarray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[addressarray[indexPath.row] valueForKey:@"company"] isEqualToString:@""])
    {
        return 179;
    }
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"Cell";
    BillingAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellNameFirst];
    if (cell == NULL)
    {
        cell = [[BillingAddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNameFirst];
    }
    
    if([selecteddict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
    {
         cell.imgradiobtn.image=[UIImage imageNamed:@"radio_btn3.png"];
    }
    else
    {
        cell.imgradiobtn.image=[UIImage imageNamed:@"radio_btn2.png"];
    }
    
#pragma condition maintain
    if([[addressarray[indexPath.row] valueForKey:@"company"] isKindOfClass:[NSNull class]]||[[addressarray[indexPath.row] valueForKey:@"company"] isEqualToString:@""])
    {
#pragma design maintain
        for (id v in cell.contentView.subviews)
        {
            if([v isKindOfClass:[UIView class]])
            {
                UIView *vw=(UIView *)v;
                
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UILabel class]])
                    {
                        UILabel *lbl=(UILabel *)v;
                        if(lbl.tag>2)
                        {
                            if(lbl.tag==3)
                            {
                                lbl.hidden=YES;
                            }
                            else
                            {
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-2)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                        }
                        
                    }
                }
            }
        }
    }
    else
    {
        for (id v in cell.contentView.subviews)
        {
            if([v isKindOfClass:[UIView class]])
            {
                UIView *vw=(UIView *)v;
                
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UILabel class]])
                    {
                        UILabel *lbl=(UILabel *)v;
                        if(lbl.tag>2)
                        {
                            if(lbl.tag==3)
                            {
                                lbl.hidden=NO;
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-1)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                            else
                            {
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-1)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                        }
                    }
                }
            }
        }
    }
    
    cell.lbladdresstitle.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"alias"]];
    cell.lbladdress.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"address1"]];
    cell.lblzip.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"postcode"]];
    cell.lblcity.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"city"]];
    cell.lblcountry.text=[NSString stringWithFormat:@"%@, %@",[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"country_name"]],[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"state_name"]]];
#pragma trimming space
    NSString *firstname=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"firstname"]];
    firstname=[firstname stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *lastname=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"lastname"]];
    lastname=[lastname stringByReplacingOccurrencesOfString:@" " withString:@""];
    cell.lblname.text=[NSString stringWithFormat:@"%@ %@",firstname,lastname];
    cell.lblcompanyname.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"company"]];
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BillingAddressTableViewCell *cell=(BillingAddressTableViewCell *)[self.tblView cellForRowAtIndexPath:indexPath];
    cell.imgradiobtn.image=[UIImage imageNamed:@"radio_btn3.png"];
    [selecteddict setValue:@"select" forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];

#pragma new code
    billingdict=[[NSMutableDictionary alloc] init];
    billingdict=addressarray[indexPath.row];
    [[SingleToneClass sharedManager] setBillinginfoInfoDict:billingdict];
    
    
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BillingAddressTableViewCell *cell=(BillingAddressTableViewCell *)[self.tblView cellForRowAtIndexPath:indexPath];
    cell.imgradiobtn.image=[UIImage imageNamed:@"radio_btn2.png"];
    [selecteddict setValue:nil forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
}
- (IBAction)didTapUpdate:(id)sender {
    
    fromnextpage=TRUE;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    [self performSegueWithIdentifier:@"pushtoeditaddress" sender:indexPath];
}
- (IBAction)didTapcheckbox:(id)sender {
    
    if(!ischecked)
    {
        self.imgcheckbox.image=[UIImage imageNamed:@"checkbox.png"];
        ischecked=TRUE;
    }
    else
    {
        self.imgcheckbox.image=[UIImage imageNamed:@"checkbox2.png"];
        ischecked=FALSE;
    }
    
}


@end
