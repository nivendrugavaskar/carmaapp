//
//  AddressViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 25/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillingAddressViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UIImageView *imgcheckbox;

@property (strong,nonatomic)NSMutableDictionary *billingdict;
@property (strong, nonatomic) IBOutlet UIView *billingaddressview;

@end
