//
//  YourProfileViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 03/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "YourProfileViewController.h"
#import "AppDelegate.h"

@interface YourProfileViewController ()<connectionprotocol>
{
    AppDelegate *app;
    Connection *conn;
    BOOL loadlist;
    
    NSString *selectedservice;
    NSMutableDictionary *profileDict;
    
    CGPoint pointtrack,pointtrackcontentoffset;
}

@end

@implementation YourProfileViewController

@synthesize keyboardToolBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setbarbuttons];
    loadlist=FALSE;
    
   #pragma set toolbar
    pointtrackcontentoffset=self.view.frame.origin;
}
#pragma set barbuttons
-(void)setbarbuttons
{
    self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;
    
#pragma left menu bar
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    self.navigationItem.leftBarButtonItems = @[leftbarbutton];
    
    
#pragma right menu bar
    [self save];
    
}
#pragma edit

-(void)edit
{
    self.navigationItem.rightBarButtonItems=nil;
    UIButton *btn2=[Custombarbutton setaddtocartbutton:@"Save"];
    [btn2 addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
    for (id v in self.ascrollView.subviews) {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *innerview=(UIView *)v;
            for (id v in innerview.subviews)
            {
                if([v isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfld=(UITextField *)v;
                    txtfld.inputAccessoryView=keyboardToolBar;
                    txtfld.userInteractionEnabled=YES;
                }
            }
        }
    }
    selectedservice=@"editdetail";
#pragma allocate
    _txtcurrentpwd.text=@"";
    _txtnewpwd.text=@"";
    [_txtfirstname becomeFirstResponder];
}

-(void)save
{
    self.navigationItem.rightBarButtonItems=nil;
    UIButton *btn2=[Custombarbutton setaddtocartbutton:@"Edit"];
    [btn2 addTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
    for (id v in self.ascrollView.subviews) {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *innerview=(UIView *)v;
            for (id v in innerview.subviews)
            {
                if([v isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfld=(UITextField *)v;
                    txtfld.inputAccessoryView=keyboardToolBar;
                    txtfld.userInteractionEnabled=NO;
                }
            }
        }
    }
    [self dismisskeyboard];
   // email,currentpasswd,firstname,lastname,newpasswd,id_customer
    NSDictionary *params;
    if(_txtcurrentpwd.text&&_txtnewpwd.text)
    {
       params=@{@"email": _txtemailaddress.text,@"currentpasswd": _txtcurrentpwd.text,@"newpasswd": _txtnewpwd.text,@"firstname": _txtfirstname.text,@"lastname": _txtlastname.text,@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
    }
    else
    {
        params=@{@"email": _txtemailaddress.text,@"currentpasswd": @"",@"newpasswd": @"",@"firstname": _txtfirstname.text,@"lastname": _txtlastname.text,@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
    }
    
    [conn servicecall:params :@"profileupdate.php"];
    
}

-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"My Profile"]];
    
    if(!loadlist)
    {
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        selectedservice=@"getprofile";
        NSDictionary *param=@{@"id_customer": [app->logindetails valueForKeyPath:@"result.id_customer"]};
        [conn servicecall:param :@"profiledetails.php"];
        
    }
    
}
#pragma handle response
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"getprofile"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
            profileDict=[connectiondict valueForKey:@"result"];
            [self loadadata];
        }
    }
    else if ([selectedservice isEqualToString:@"editdetail"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
            UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:nil message:[connectiondict valueForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alrt show];
            
#pragma save user details
            if([NSString validation:_txtcurrentpwd.text]&&[NSString validation:_txtnewpwd.text])
            {
             [[NSUserDefaults standardUserDefaults] setValue:_txtnewpwd.text forKey:@"pwd"];
            }
            if([NSString validation:_txtemailaddress.text])
            {
               [[NSUserDefaults standardUserDefaults] setValue:_txtemailaddress.text forKey:@"useremail"];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}
#pragma load data
-(void)loadadata
{
     _txtfirstname.text=[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"firstname"]];
     _txtlastname.text=[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"lastname"]];
     _txtemailaddress.text=[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"email"]];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma textfield delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack = textField.superview.frame.origin;
    
    CGRect rect=self.view.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(SCREEN_HEIGHT==480||SCREEN_HEIGHT==568)
    {
        if(pointtrack.y>120)
        {
            if(pointtrack.y>350)
            {
                pointtrack.y=340;
            }
            rect.origin.y-=pointtrack.y-100;
        }
    }
    
    else if(SCREEN_HEIGHT==667)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-100;
        }
    }
    else if(SCREEN_HEIGHT==736)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-120;
        };
    }
    else
    {
        
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
    return NO;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(!(self.activeTextField.tag==2||self.activeTextField.tag==3))
    {
        if([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    return YES;
}

#pragma dismiss keyboard
-(void)dismisskeyboard
{
    [self.activeTextField resignFirstResponder];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
}

- (IBAction)didTapnext:(id)sender {
    NSInteger nextTag = self.activeTextField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapPrevious:(id)sender {
    
    NSInteger nextTag = self.activeTextField.tag - 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapDone:(id)sender {
    [self dismisskeyboard];
}
- (IBAction)didTapChangePwd:(id)sender
{
        [self dismisskeyboard];
        [self createcustomalert];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==101)
    {
        if(buttonIndex==1)
        {
            if(![NSString validationwithouttrim:[alertView textFieldAtIndex:0].text])
            {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"Current password can't empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag=102;
                [alert show];
                
            }
            else if(![NSString validationwithouttrim:[alertView textFieldAtIndex:1].text])
            {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Wrong" message:@"New password can't empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag=102;
                [alert show];
                
            }
            else
            {
                _txtcurrentpwd=[alertView textFieldAtIndex:0];
                _txtnewpwd=[alertView textFieldAtIndex:1];
                
              selectedservice=@"editdetail";
              [self save];
            }
        }
    }
    else if(alertView.tag==102)
    {
        [self createcustomalert];
    }
}
-(void)createcustomalert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Change Password" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit", nil];
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [alert textFieldAtIndex:0].keyboardType=UIKeyboardTypeDefault;
    [alert textFieldAtIndex:0].secureTextEntry=YES;
    [alert textFieldAtIndex:0].placeholder=@"Enter current password";
    [alert textFieldAtIndex:1].keyboardType=UIKeyboardTypeDefault;
    [alert textFieldAtIndex:1].placeholder=@"Enter New Password";
    [alert textFieldAtIndex:1].secureTextEntry=YES;
    alert.tag=101;
    [alert show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
