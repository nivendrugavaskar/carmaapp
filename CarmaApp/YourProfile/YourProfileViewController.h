//
//  YourProfileViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 03/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YourProfileViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtfirstname;
@property (strong, nonatomic) IBOutlet UITextField *txtlastname;
@property (strong, nonatomic) IBOutlet UITextField *txtemailaddress;
@property (strong, nonatomic) IBOutlet UIToolbar *keyboardToolBar;
@property (strong, nonatomic) IBOutlet UIScrollView *ascrollView;

#pragma custom text field track
@property(weak,nonatomic)UITextField *txtcurrentpwd;
@property(weak,nonatomic)UITextField *txtnewpwd;

@property(weak,nonatomic)UITextField *activeTextField;
@end
