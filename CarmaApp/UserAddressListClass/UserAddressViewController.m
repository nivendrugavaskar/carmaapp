//
//  UserAddressViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 01/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "UserAddressViewController.h"
#import "AppDelegate.h"
#import "UserAddressTableViewCell.h"
#import "EditAddresViewController.h"




@interface UserAddressViewController ()<UITableViewDataSource,UITableViewDelegate,connectionprotocol,listingaddressprotocal,UIAlertViewDelegate>
{
    Connection *conn;
    AppDelegate *app;
    NSMutableArray *addressarray;
    BOOL loadpage,company,fromnextpage;
    NSString *selectedservice;
    NSIndexPath *deletedindex;
}

@end

@implementation UserAddressViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
     [self setbarbuttons];
    
    fromnextpage=FALSE;
    


    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma set barbuttons
-(void)setbarbuttons
{
    self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;
    
#pragma left menu bar
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    self.navigationItem.leftBarButtonItems = @[leftbarbutton];
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"My Addresses"]];
    
    if(!fromnextpage)
    {
#pragma fetching user address
        selectedservice=@"Fetchaddress";
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        NSString *customerid=[NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]];
        NSDictionary *params=@{@"id_customer": customerid};
        [conn servicecall:params :@"addresslist.php"];
    }
    
}
#pragma response delegate
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"Fetchaddress"])
    {
    if(!connectiondict)
    {
        return;
    }
    else
    {
        addressarray=[[NSMutableArray alloc] init];
        addressarray=[[connectiondict valueForKey:@"result"] mutableCopy];
        if(addressarray.count==0)
        {
            
            [self performSegueWithIdentifier:@"pushtoeditaddress" sender:self];
        }
        else
        {
            [self.tblView reloadData];
        }
    }
    }
    else if([selectedservice isEqualToString:@"Deleteaddress"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
            [addressarray removeObjectAtIndex:deletedindex.row];
            [self.tblView deleteRowsAtIndexPaths:@[deletedindex] withRowAnimation:UITableViewRowAnimationLeft];
            [self.tblView reloadData];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Deleted" message:[connectiondict valueForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=101;
            [alert show];
            
            
        }
        
    }
}
#pragma alert view delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
            deletedindex=[[NSIndexPath alloc] init];
        }
        else
        {
            selectedservice=@"Deleteaddress";
            NSString *addresid=[NSString stringWithFormat:@"%@",[addressarray[deletedindex.row] valueForKeyPath:@"id_address"]];
            NSDictionary *params=@{@"id_address": addresid};
            [conn servicecall:params :@"deleteaddress.php"];

        }
    }
    else if(alertView.tag==101)
    {
        if(addressarray.count==0)
        {
            [self performSegueWithIdentifier:@"pushtoeditaddress" sender:self];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}
#pragma update table view from edit page
- (void)buttonTappedsave
{
    fromnextpage=FALSE;
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoeditaddress"])
    {
        EditAddresViewController *inst=(EditAddresViewController *)segue.destinationViewController;
        if([sender isKindOfClass:[NSIndexPath class]])
        {
          NSIndexPath *indexpath=(NSIndexPath *)sender;
         inst.addressDict=addressarray[indexpath.row];
         inst.selectedaddress=@"update";
         inst.checkaddressarraycount=(int)addressarray.count;
        }
        else
        {
        inst.checkaddressarraycount=(int)addressarray.count;
        inst.selectedaddress=@"Add";
        }
        
        inst.editdelegate=self;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return addressarray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([[addressarray[indexPath.row] valueForKey:@"company"] isEqualToString:@""])
    {
        return 179;
    }
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"Cell";
    UserAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellNameFirst];
    if (cell == NULL)
    {
        cell = [[UserAddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNameFirst];
    }
    
#pragma condition maintain
    if([[addressarray[indexPath.row] valueForKey:@"company"] isKindOfClass:[NSNull class]]||[[addressarray[indexPath.row] valueForKey:@"company"] isEqualToString:@""])
    {
#pragma design maintain
        for (id v in cell.contentView.subviews)
        {
            if([v isKindOfClass:[UIView class]])
            {
                UIView *vw=(UIView *)v;
                
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UILabel class]])
                    {
                        UILabel *lbl=(UILabel *)v;
                        if(lbl.tag>2)
                        {
                            if(lbl.tag==3)
                            {
                                lbl.hidden=YES;
                            }
                            else
                            {
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-2)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                        }
                        
                    }
                }
            }
        }
    }
    else
    {
        for (id v in cell.contentView.subviews)
        {
            if([v isKindOfClass:[UIView class]])
            {
                UIView *vw=(UIView *)v;
                
                for (id v in vw.subviews)
                {
                    if([v isKindOfClass:[UILabel class]])
                    {
                        UILabel *lbl=(UILabel *)v;
                        if(lbl.tag>2)
                        {
                            if(lbl.tag==3)
                            {
                                lbl.hidden=NO;
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-1)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                            else
                            {
                                lbl.frame=CGRectMake(lbl.frame.origin.x,5+((lbl.tag-1)*21), lbl.frame.size.width, lbl.frame.size.height);
                            }
                        }
                    }
                }
            }
        }
    }
    
    cell.lbladdresstitle.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"alias"]];
    cell.lbladdress.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"address1"]];
    cell.lblzip.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"postcode"]];
    cell.lblcity.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"city"]];
    cell.lblcountry.text=[NSString stringWithFormat:@"%@, %@",[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"country_name"]],[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"state_name"]]];
#pragma trimming space
    NSString *firstname=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"firstname"]];
    firstname=[firstname stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *lastname=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"lastname"]];
    lastname=[lastname stringByReplacingOccurrencesOfString:@" " withString:@""];
    cell.lblname.text=[NSString stringWithFormat:@"%@ %@",firstname,lastname];
    cell.lblcompanyname.text=[NSString stringWithFormat:@"%@",[addressarray[indexPath.row] valueForKey:@"company"]];
    
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // NSLog(@"last index path=%ld",(long)lastindexpath.row);
    //[self performSegueWithIdentifier:@"pustToproduct" sender:self];
    
}

//- (UIView *)  tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
// {
//
//    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblView.frame.size.width, 30)];
//    footerView.backgroundColor=[UIColor clearColor];
//    footerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    UIButton *anotheraddress = nil;
//    if ([tableView isEqual:self.tblView] &&
//        section == 0)
//    {
//        if([addressarray count]>0)
//        {
//            
//            anotheraddress = [[UIButton alloc] initWithFrame:CGRectMake(5,0,self.tblView.frame.size.width-10, 30)];
//            [anotheraddress.titleLabel setFont:[UIFont fontWithName:@"Philosopher" size:16.0f]];
//            anotheraddress.titleLabel.textAlignment=NSTextAlignmentCenter;
//            anotheraddress.backgroundColor=[UIColor whiteColor];
//            [anotheraddress setTitle:@"Add Another Address" forState:UIControlStateNormal];
//            [anotheraddress setTitleColor:[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f] forState:UIControlStateNormal];
//            [anotheraddress addTarget:self action:@selector(addaddress) forControlEvents:UIControlEventTouchUpInside];
//            [footerView addSubview:anotheraddress];
//            
//        }
//    }
//    return footerView;
//}

- (IBAction)didTapUpdate:(id)sender
{
    fromnextpage=TRUE;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    [self performSegueWithIdentifier:@"pushtoeditaddress" sender:indexPath];
    
}
- (IBAction)didTapDelete:(id)sender {
   
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    deletedindex=indexPath;
    
    UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Do you want to delete address?" message:[addressarray[indexPath.row] valueForKeyPath:@"alias"] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alrt.tag=100;
    [alrt show];
    
    
    
}

- (IBAction)addaddress:(id)sender {
    
    fromnextpage=TRUE;
    [self performSegueWithIdentifier:@"pushtoeditaddress" sender:self];
}


@end
