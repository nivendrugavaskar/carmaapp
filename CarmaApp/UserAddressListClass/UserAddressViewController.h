//
//  UserAddressViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 01/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserAddressViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tblView;

@end
