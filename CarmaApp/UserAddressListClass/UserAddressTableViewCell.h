//
//  UserAddressTableViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 01/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserAddressTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbladdresstitle;
@property (strong, nonatomic) IBOutlet UILabel *lbladdress;
@property (strong, nonatomic) IBOutlet UILabel *lblzip;
@property (strong, nonatomic) IBOutlet UILabel *lblcity;
@property (strong, nonatomic) IBOutlet UILabel *lblcountry;
@property (strong, nonatomic) IBOutlet UILabel *lblname;
@property (strong, nonatomic) IBOutlet UILabel *lblcompanyname;
@property (strong, nonatomic) IBOutlet UIButton *btndelete;
@property (strong, nonatomic) IBOutlet UIButton *btnupdate;

@end
