//
//  ReviewListViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 29/09/15.
//  Copyright © 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol reviewlistprotocol
- (void)buttonclick;
@end

@interface ReviewListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
}
@property (strong, nonatomic) IBOutlet UITableView *tblView;

@property (strong,nonatomic)NSMutableDictionary *productdict;

@property (assign) id <reviewlistprotocol> reviewdelegate;
@end
