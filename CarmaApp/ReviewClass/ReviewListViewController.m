//
//  ReviewListViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 29/09/15.
//  Copyright © 2015 NivendruGavaskar. All rights reserved.
//

#import "ReviewListViewController.h"
#import "AppDelegate.h"
#import "ReviewListTableViewCell.h"
#import "StarRatingView.h"
#import "FBSliderMenuViewController.h"

#define kLabelAllowance 50.0f
#define kStarViewHeight 10.0f
#define kStarViewWidth 50.0f
#define kLeftPadding 5.0f

@interface ReviewListViewController ()<connectionprotocol>
{
    AppDelegate *app;
    UILabel *badgeno;
    float cellheight,selectedcellheight;
    NSMutableDictionary *selectedIndexes;
    NSMutableArray *reviewArray;
    
    Connection *conn;
}

@end

@implementation ReviewListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.reviewdelegate =(FBSliderMenuViewController *)self.menuContainerViewController.leftMenuViewController;
    
    [self setupbarbutton];
     selectedIndexes = [[NSMutableDictionary alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupbarbutton
{
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
    
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setcartbutton];
#pragma badge circle
    UIImageView *badge=[[UIImageView alloc] initWithFrame:btn2.bounds];
    badge.frame=CGRectMake(16, -3, 20, 20);
    badge.contentMode=UIViewContentModeScaleAspectFit;
    badge.image=[UIImage imageNamed:@"circle_icon.png"];
#pragma number label
    badgeno=[[UILabel alloc] initWithFrame:badge.bounds];
    badgeno.frame=CGRectMake(2, 2, 16, 16);
    badgeno.font=[UIFont fontWithName:@"Philosopher" size:12.0f];
    badgeno.textAlignment=NSTextAlignmentCenter;
    badgeno.text=app->totalproduct;
    badgeno.textColor=[UIColor whiteColor];
    [badge addSubview:badgeno];
    // end
    [btn2 addSubview:badge];
    [btn2 addTarget:self action:@selector(cartbutton) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Reviews"]];
    
    NSLog(@"Service call");
#pragma category list service
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    NSMutableDictionary *params=[NSMutableDictionary dictionaryWithObjectsAndKeys:[_productdict valueForKey:@"id_product"],@"id_product", nil];
    [conn servicecall:params :@"showreview.php"];
   
    
}
#pragma response delegate
-(void)sucessdone:(NSDictionary *)connectiondict
{
    if(!connectiondict)
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        return;
    }
    else
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        reviewArray=[[NSMutableArray alloc] init];
        reviewArray=[connectiondict valueForKeyPath:@"result"];
        [self.tblView reloadData];
        
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)cartbutton
{
    
   [self.reviewdelegate buttonclick];
    
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

// new method for dynamic height
-(CGSize)dyanamiclabel:(UILabel *)takelabel
{
    
    CGSize constrainedSize = CGSizeMake(takelabel.frame.size.width  , MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          takelabel.font, NSFontAttributeName,
                                          nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:takelabel.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = takelabel.frame;
    newFrame.size.height = requiredHeight.size.height;
    takelabel.frame = newFrame;
    return takelabel.frame.size;
}
#pragma tableview delegate and data source
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Deselect cell
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    // Toggle 'selected' state
    BOOL isSelected = ![self cellIsSelected:indexPath];
    if(isSelected)
    {
        [selectedIndexes setValue:@"select" forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    }
    else
    {
        [selectedIndexes setValue:@"NO" forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    }
    // This is where magic happens...
    [self.tblView beginUpdates];
    [self.tblView endUpdates];
    // [self.aTableview reloadData];
    [self.tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellNameFirst = @"Cell";
    
    ReviewListTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:cellNameFirst forIndexPath:indexPath];
    
    
    @try
    {
        cell.reviewtitle.text=[[reviewArray objectAtIndex:indexPath.row] valueForKey:@"title"];
        cell.customername.text=[[reviewArray objectAtIndex:indexPath.row] valueForKey:@"customer_name"];
        //cell.dynamicfeedbacklabel.backgroundColor=[UIColor greenColor];
        cell.dynamicfeedbacklabel.text=[[reviewArray objectAtIndex:indexPath.row] valueForKey:@"content"];
//        // new code
//        StarRatingView* starViewNoLabel = [[StarRatingView alloc]initWithFrame:CGRectMake(60, cell.ratinglabel.frame.origin.y, kStarViewWidth+kLeftPadding, kStarViewHeight) andRating:(int)[[[reviewArray objectAtIndex:indexPath.row] valueForKey:@"grade"] integerValue]*20 withLabel:NO animated:NO];
        
        cell.starratingview=[cell.starratingview initWithFrame:cell.starratingview.frame andRating:(int)[[[reviewArray objectAtIndex:indexPath.row] valueForKey:@"grade"] integerValue]*20 withLabel:NO animated:NO];
       // starViewNoLabel.userInteractionEnabled=NO;
       // [cell.contentView addSubview:starViewNoLabel];
        cell.starratingview.userInteractionEnabled=NO;
        
        
        if([self cellIsSelected:indexPath])
        {
            cell.morelabel.hidden=TRUE;
            CGSize newget=[self dyanamiclabel:cell.dynamicfeedbacklabel];
            
            if(newget.height>20.0&&newget.height<30.0)
            {
                cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width,30);
                
                
            }
            else if(newget.height<20.0)
            {
                cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width,20);
                
                
            }
            else if(newget.height>30.0)
            {
                cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width,newget.height);
                
                cell.morelabel.hidden=FALSE;
                cell.morelabel.text=@"Less....";
                
            }
            
            
            
        }
        else
        {
            
            CGSize newget=[self dyanamiclabel:cell.dynamicfeedbacklabel];
            if(newget.height>20.0&&newget.height<30.0)
            {
                cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width,30);
                cell.morelabel.hidden=TRUE;
                
            }
            else if(newget.height<20.0)
            {
                cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width,20);
                cell.morelabel.hidden=TRUE;
                
            }
            else if(newget.height>30.0)
            {
                cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width,30);
                cell.morelabel.hidden=FALSE;
                cell.morelabel.text=@"More....";
            }
            
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [reviewArray count];
}

- (BOOL)cellIsSelected:(NSIndexPath *)indexPath
{
    if([[selectedIndexes valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]isEqualToString:@"select"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)viewDidUnload
{
    selectedIndexes = nil;
    [super viewDidUnload];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    ReviewListTableViewCell *cell=(ReviewListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    
    cell.dynamicfeedbacklabel.text=[[reviewArray objectAtIndex:indexPath.row] valueForKey:@"content"];
    // If our cell is selected, return double height
    if([self cellIsSelected:indexPath])
    {
        CGSize newget=[self dyanamiclabel:cell.dynamicfeedbacklabel];
        selectedcellheight=cell.ratinglabel.frame.origin.y+cell.ratinglabel.frame.size.height+newget.height+30;
        cell.dynamicfeedbacklabel.frame = CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y, newget.width, newget.height);
        if(newget.height>20.0&&newget.height<30.0)
        {
            cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width-10,30);
            cell.morelabel.hidden=TRUE;
            selectedcellheight=125.0;
            
        }
        else if(newget.height<20.0)
        {
            cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width-10,20);
            cell.morelabel.hidden=TRUE;
            selectedcellheight=115.0;
            
        }
        else if(newget.height>30.0)
        {
            cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width-10,newget.height);
            cell.morelabel.hidden=TRUE;
            // new code
            
        }
        return selectedcellheight;
    }
    
    // Cell isn't selected so return single height
    CGSize newget=[self dyanamiclabel:cell.dynamicfeedbacklabel];
    if(newget.height>20.0&&newget.height<30.0)
    {
        cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width-10,30);
        cell.morelabel.hidden=TRUE;
        cellheight=125.0;
        
    }
    else if(newget.height<20.0)
    {
        cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width-10,20);
        cell.morelabel.hidden=TRUE;
        cellheight=115.0;
        
    }
    else if(newget.height>30.0)
    {
        cell.dynamicfeedbacklabel.frame=CGRectMake(cell.dynamicfeedbacklabel.frame.origin.x, cell.dynamicfeedbacklabel.frame.origin.y,cell.dynamicfeedbacklabel.frame.size.width,30);
        cell.morelabel.hidden=FALSE;
        cellheight=125.0;
        
        // new code
        
    }
    
    return cellheight;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
