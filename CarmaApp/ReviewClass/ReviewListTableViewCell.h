//
//  ReviewListTableViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 29/09/15.
//  Copyright © 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarRatingView.h"

@interface ReviewListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *dynamicfeedbacklabel;
@property (strong, nonatomic) IBOutlet UILabel *morelabel;
@property (strong, nonatomic) IBOutlet UILabel *reviewtitle;
@property (strong, nonatomic) IBOutlet UILabel *customername;
@property (strong, nonatomic) IBOutlet StarRatingView *starratingview;
@property (strong, nonatomic) IBOutlet UILabel *ratinglabel;
@end
