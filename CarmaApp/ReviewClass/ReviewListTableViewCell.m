//
//  ReviewListTableViewCell.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 29/09/15.
//  Copyright © 2015 NivendruGavaskar. All rights reserved.
//

#import "ReviewListTableViewCell.h"

@implementation ReviewListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
