//
//  ViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 05/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnFB;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPwd;
@property (strong, nonatomic) IBOutlet UIToolbar *keyBoardToolBar;

@property(strong,nonatomic)UITextField *activeTextField;

@end

