//
//  ViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 05/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"



@interface ViewController ()<FBSDKAppInviteDialogDelegate,connectionprotocol,UIAlertViewDelegate>
{
    AppDelegate *app;
    Connection *conn;
    NSMutableDictionary *loginDict;
    NSString *selectedservice;
    CGPoint pointtrack,pointtrackcontentoffset;
}

@end

@implementation ViewController
@synthesize keyBoardToolBar;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
   // [self preferredStatusBarStyle];
    
  //  [self setNeedsStatusBarAppearanceUpdate];
    
    if ([FBSDKAccessToken currentAccessToken])
    {
        
    #pragma fetch user info
        [SVProgressHUD showWithStatus:@"Please Wait...."];
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"id,name,email" forKey:@"fields"];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                // NSLog(@"%@",result);
                 NSLog(@"fetched user in didload :%@  and Email : %@", result,result[@"email"]);
                 [SVProgressHUD dismiss];
                 [self performSegueWithIdentifier:@"pushtocategory" sender:self];
//                  [self performSegueWithIdentifier:@"pushtocategory" sender:self];
                 
             }
         }];
        
        
    }
    else if([[[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginType"] mutableCopy] isEqualToString:@"emaillogin"])
    {
      [self performSegueWithIdentifier:@"pushtocategory" sender:self];
    }
    else
    {
    
#pragma set fame to manage animation
    pointtrackcontentoffset=self.view.frame.origin;
    
    for (id v in self.view.subviews) {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *innerview=(UIView *)v;
            for (id v in innerview.subviews)
            {
                if([v isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfld=(UITextField *)v;
                    txtfld.inputAccessoryView=keyBoardToolBar;
                }
            }
        }
    }
    }
}

#pragma optional for now
-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results
{
    
}
-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
      [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Sign In"]];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapCreateAccount:(id)sender
{
    [self performSegueWithIdentifier:@"pushtoRegister" sender:self];
}
- (IBAction)didTapLogin:(id)sender
{
   
    
        [self dismisskeyboard];
        for (id v in self.view.subviews) {
            if([v isKindOfClass:[UIView class]])
            {
                UIView *innerview=(UIView *)v;
                for (id v in innerview.subviews)
                {
                    if([v isKindOfClass:[UITextField class]])
                    {
                        UITextField *txtfld=(UITextField *)v;
                        if(![NSString validation:txtfld.text])
                        {
                            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"All fields are mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            return;
                        }
                        else if(txtfld.tag==1)
                        {
                            if(![NSString validateEmail:txtfld.text])
                            {
                                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Incorrect" message:@"Please enter correct email address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                [alert show];
                                return;
                            }
                        }
                    }
                }
                
            }
        }
    
        selectedservice=@"Login";
        
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        //email,firstname,lastname,passwd
        NSDictionary *params=@{@"email": _txtEmail.text,@"passwd": _txtPwd.text};
        [conn servicecall : params : @"customer_login.php"];
    
}
#pragma textfield delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack = textField.superview.frame.origin;
    
    CGRect rect=self.view.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(SCREEN_HEIGHT==480||SCREEN_HEIGHT==568)
    {
        if(pointtrack.y>120)
        {
            if(pointtrack.y>350)
            {
                pointtrack.y=340;
            }
            rect.origin.y-=pointtrack.y-130;
        }
    }
    
    else if(SCREEN_HEIGHT==667)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-100;
        }
    }
    else if(SCREEN_HEIGHT==736)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-120;
        };
    }
    else
    {
        
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
    return NO;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(!(self.activeTextField.tag==1||self.activeTextField.tag==2))
    {
        if([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    return YES;
}

#pragma dismiss keyboard
-(void)dismisskeyboard
{
    [self.activeTextField resignFirstResponder];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
}

- (IBAction)didTapnext:(id)sender {
    NSInteger nextTag = self.activeTextField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapPrevious:(id)sender {
    
    NSInteger nextTag = self.activeTextField.tag - 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapDone:(id)sender {
    [self dismisskeyboard];
}

-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"Login"])
    {
        
        loginDict=[[NSMutableDictionary alloc] initWithDictionary:connectiondict];
#pragma updating details
        app->logindetails=[[NSMutableDictionary alloc] initWithDictionary:loginDict];
#pragma totalproduct
        app->totalproduct=[NSString stringWithFormat:@"%@",[loginDict valueForKeyPath:@"result.total_product_in_cart"]];
        
#pragma save user details
        NSString *useremail=[loginDict valueForKeyPath:@"result.email"];
        NSString *pwd=_txtPwd.text;
        [[NSUserDefaults standardUserDefaults] setValue:useremail forKey:@"useremail"];
        [[NSUserDefaults standardUserDefaults] setValue:pwd forKey:@"pwd"];
        [[NSUserDefaults standardUserDefaults] setValue:@"emaillogin" forKey:@"LoginType"];
        // new code to save dict
#pragma save dictionary
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:app->logindetails];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"loggedindetails"];
        [[NSUserDefaults standardUserDefaults] setObject:app->totalproduct forKey:@"cartbadge"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self performSegueWithIdentifier:@"pushtocategory" sender:self];
        
    }
    else if([selectedservice isEqualToString:@"ForgetPwd"])
    {
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Mail sent" message:[connectiondict valueForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    
}

- (IBAction)didTapFB_Click:(id)sender
{
    
    
    if (![FBSDKAccessToken currentAccessToken])
    {
    [SVProgressHUD showWithStatus:@"Please Wait...."];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
   // [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
        {
        if (error) {
            [SVProgressHUD dismiss];
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
            [SVProgressHUD dismiss];
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            NSLog(@"%@",result);
            if ([result.grantedPermissions containsObject:@"email"])
            {
                // Do work
                NSLog(@"Granted all permission");
                
                 //NSLog(@"fetched Email : %@",[result.grantedPermissions valueForKey:@"contact_email"]);
                if ([FBSDKAccessToken currentAccessToken])
                {
                    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                    [parameters setValue:@"id,name,email" forKey:@"fields"];
                    
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                     {
                         if (!error)
                         {
                             [SVProgressHUD dismiss];
                             NSLog(@"fetched user when no login:%@  and Email : %@", result,result[@"email"]);
                             [[NSUserDefaults standardUserDefaults] setValue:result[@"email"] forKey:@"useremail"];
                             [[NSUserDefaults standardUserDefaults] setValue:@"fblogin" forKey:@"LoginType"];
                             [[NSUserDefaults standardUserDefaults] synchronize];
                              [self performSegueWithIdentifier:@"pushtocategory" sender:self];
                         }
                     }];
                }
            else
            {
                NSLog(@"Not granted");
            }
                
            }
        }
    }];
    }
    
//    if ([FBSDKAccessToken currentAccessToken])
//    {
//            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
//                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id user, NSError *error)
//            {
//                         if (!error)
//                         {
//                                 NSDictionary *dictUser = (NSDictionary *)user;
//                                 // This dictionary contain user Information which is possible to get from Facebook account.
//                                 NSLog(@"User...%@",dictUser);
//                                 //[self loginViaFacebook:dictUser];
//                             }}];
//    }
//    else
//    {
//        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//            [login logInWithReadPermissions:@[@"public_profile"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//            if (error) {
//                // Process error
//            }
//            else if (result.isCancelled)
//            {
//                // Handle cancellations
//            }
//            else
//            {
//                
//                if ([FBSDKAccessToken currentAccessToken])
//                {
//                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
//                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id user, NSError *error)
//                    {
//                        if (!error)
//                        {
//                            NSDictionary *dictUser = (NSDictionary *)user;
//                            // This dictionary contain user Information which is possible to get from Facebook account.
//                            NSLog(@"User...%@",dictUser);
//                           // [self loginViaFacebook:dictUser];
//                        }
//                    }];
//                }
//            }
//        }];
//    }
}
- (IBAction)didTapFgtPwd:(id)sender {
    
    [self createcustomalert];
    
}

-(void)createcustomalert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Forgot Password" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].keyboardType=UIKeyboardTypeEmailAddress;
    [alert textFieldAtIndex:0].placeholder=@"Enter your email";
    alert.tag=101;
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==101)
    {
        if(buttonIndex==1)
        {
            if(![NSString validation:[alertView textFieldAtIndex:0].text])
            {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"Email can't empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag=102;
                [alert show];
                
            }
            else if(![NSString validateEmail:[alertView textFieldAtIndex:0].text])
            {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Incorrect" message:@"Email id is incorrect" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag=102;
                [alert show];
                
            }
            else
            {
                selectedservice=@"ForgetPwd";
                conn=[[Connection alloc] init];
                conn.connectionDelegate=self;
                //email,firstname,lastname,passwd
                NSDictionary *params=@{@"email": [alertView textFieldAtIndex:0].text};
                [conn servicecall : params : @"forgetpassword.php"];
            }
        }
    }
    else if(alertView.tag==102)
    {
        [self createcustomalert];
    }
}

@end
