//
//  FBSliderMenuViewController.m
//  FBSlider
//  Created by Subhr Roy on 11/02/14.
//  Copyright (c) 2014 Subhr Roy. All rights reserved.
//

#import "FBSliderMenuViewController.h"
#import "MFSideMenu.h"
#import "SliderTableViewCell.h"
#import "CategoryViewController.h"
#import "ViewController.h"
#import "UserAddressViewController.h"
#import "YourProfileViewController.h"
#import  "WishListViewController.h"
#import "CarmaStoreViewController.h"
#import "MyCartViewController.h"
//#import "OrderHistoryViewController.h"
//#import "CartDetailsViewController.h"
//

#import "ProductViewController.h"

@interface FBSliderMenuViewController ()
{
    
    NSMutableArray *arraylist;
    NSMutableArray *arrayImage,*arrayHoverImage;
    
    NSIndexPath *lastindexpath,*notchangeindex;
    
}

@end

@implementation FBSliderMenuViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)buttonclick
{
    [self.menuTable deselectRowAtIndexPath:lastindexpath animated:YES];
    SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:lastindexpath];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.lbl_List.textColor=[UIColor blackColor];
    cell.img_Icon.image=arrayImage[lastindexpath.row];
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:2 inSection:0];
    
    if(indexPath.row==8)
    {
        [self.menuTable deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    if(!lastindexpath)
    {
        lastindexpath=0;
    }
    
    if (![indexPath compare:lastindexpath] == NSOrderedSame)
    {
        if(indexPath.row==8)
        {
            [self.menuTable cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor clearColor];
            NSIndexPath *ind=[NSIndexPath indexPathForRow:0 inSection:0];
            [self.menuTable cellForRowAtIndexPath:ind].contentView.backgroundColor = [UIColor lightGrayColor];
            SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:ind];
            cell.lbl_List.textColor=[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f];
            cell.img_Icon.image=arrayHoverImage[ind.row];
            
        }
        else
        {
            [self.menuTable cellForRowAtIndexPath:lastindexpath].contentView.backgroundColor = [UIColor clearColor];
            SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:lastindexpath];
            cell.lbl_List.textColor=[UIColor blackColor];
            cell.img_Icon.image=arrayImage[lastindexpath.row];
        }
        
        
    }
#pragma maintain on click logout
    if(indexPath.row==8)
    {
        [self.menuTable cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor clearColor];
        SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:indexPath];
        cell.lbl_List.textColor=[UIColor blackColor];
        cell.img_Icon.image=arrayImage[indexPath.row];
        lastindexpath=[NSIndexPath indexPathForRow:0 inSection:0];
        
    }
    else
    {
        [self.menuTable cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor lightGrayColor];
        SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:indexPath];
        cell.lbl_List.textColor=[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f];
        lastindexpath=indexPath;
        cell.img_Icon.image=arrayHoverImage[indexPath.row];
    }
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self  selectViewControllerWithIndex:(int)indexPath.row];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    
   
    
    self.navigationController.navigationBarHidden=YES;
    
    arraylist=[[NSMutableArray alloc] initWithObjects:@"Carma Collection",@"My Addresses",@"My Cart",@"My Wish list",@"My Orders",@"My Profile",@"My Vouchers",@"Carma Store",@"Logout",nil];
    
    arrayImage=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"Carma Collections_icon.png"],[UIImage imageNamed:@"address_icon.png"],[UIImage imageNamed:@"Your Cart_icon.png"],[UIImage imageNamed:@"Your Wish list_icon.png"],[UIImage imageNamed:@"Your Orders_icon.png"],[UIImage imageNamed:@"Your Profile_icon.png"],[UIImage imageNamed:@"Your Vouchers_icon.png"],[UIImage imageNamed:@"Carma Store_icon.png"],[UIImage imageNamed:@"Logout_icon.png"],nil];
    
    arrayHoverImage=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"Carma Collections_hover_icon.png"],[UIImage imageNamed:@"address_hover_icon.png"],[UIImage imageNamed:@"Your Cart_hover_icon.png"],[UIImage imageNamed:@"Your Wish list_hover_icon.png"],[UIImage imageNamed:@"Your Orders_hover_icon.png"],[UIImage imageNamed:@"Your Profile_hover_icon.png"],[UIImage imageNamed:@"Your Vouchers_hover_icon.png"],[UIImage imageNamed:@"Carma Store_hover_icon.png"],[UIImage imageNamed:@"Logout_hover_icon.png"],nil];
    
    
    

}

-(void)selectcart:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"cartpage"]) {
        NSLog(@"Got notificaton");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectcart:) name:@"cartpage" object:self];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arraylist.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==10)
    {
        return 1000;
    }
    return 52;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"listCell";
    
    SliderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellNameFirst];
    
    
    if (cell == NULL)
    {
        cell = [[SliderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNameFirst];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if(!lastindexpath)
    {
        if(indexPath.row==0)
        {
            
            cell.contentView.backgroundColor = [UIColor lightGrayColor];
            cell.lbl_List.textColor=[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f];
            cell.img_Icon.image=[UIImage imageNamed:@"Carma Collections_hover_icon"];
            lastindexpath=indexPath;
            
        }
    }
//    UIView *selected = [[UIView alloc]initWithFrame:cell.frame];
//    [selected setBackgroundColor:[UIColor redColor]];
//    cell.selectedBackgroundView =  selected;
  
    cell.contentView.backgroundColor=[UIColor clearColor];
    cell.lbl_List.text=arraylist[indexPath.row];
    cell.img_Icon.image=arrayImage[indexPath.row];
    return cell;
}


#pragma mark -
#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"displaycell=%ld",indexPath.row);
    SliderTableViewCell *cell1=(SliderTableViewCell *)cell;
    if(lastindexpath.row==indexPath.row)
    {
        cell1.contentView.backgroundColor = [UIColor lightGrayColor];
        cell1.lbl_List.textColor=[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f];
         cell1.img_Icon.image=arrayHoverImage[indexPath.row];
        
    }
    else
    {
        
        cell1.contentView.backgroundColor = [UIColor clearColor];
        cell1.lbl_List.textColor=[UIColor blackColor];
         cell1.img_Icon.image=arrayImage[indexPath.row];
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // NSLog(@"last index path=%ld",(long)lastindexpath.row);
    if(indexPath.row==8)
    {
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    if(!lastindexpath)
    {
        lastindexpath=0;
    }
    
    if (![indexPath compare:lastindexpath] == NSOrderedSame)
    {
        if(indexPath.row==8)
        {
            [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor clearColor];
            NSIndexPath *ind=[NSIndexPath indexPathForRow:0 inSection:0];
            [tableView cellForRowAtIndexPath:ind].contentView.backgroundColor = [UIColor lightGrayColor];
            SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:ind];
            cell.lbl_List.textColor=[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f];
            cell.img_Icon.image=arrayHoverImage[ind.row];

        }
        else
        {
            [tableView cellForRowAtIndexPath:lastindexpath].contentView.backgroundColor = [UIColor clearColor];
            SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:lastindexpath];
            cell.lbl_List.textColor=[UIColor blackColor];
             cell.img_Icon.image=arrayImage[lastindexpath.row];
        }
       
        
    }
#pragma maintain on click logout
    if(indexPath.row==8)
    {
       [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor clearColor];
        SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lbl_List.textColor=[UIColor blackColor];
        cell.img_Icon.image=arrayImage[indexPath.row];
        lastindexpath=[NSIndexPath indexPathForRow:0 inSection:0];
        
    }
    else
    {
      [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor lightGrayColor];
        SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lbl_List.textColor=[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f];
        lastindexpath=indexPath;
        cell.img_Icon.image=arrayHoverImage[indexPath.row];
    }
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self  selectViewControllerWithIndex:(int)indexPath.row];
   
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"deselect index path=%ld",(long)indexPath.row);
    SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.lbl_List.textColor=[UIColor blackColor];
    cell.img_Icon.image=arrayImage[indexPath.row];
    
    
    
}


-(void)selectViewControllerWithIndex:(int)_index
{
    switch (_index) {
           
                    case 0:
                    {
                       
                        CategoryViewController *categoryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryViewController"];
                        
                                    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                                    NSArray *controllers = @[categoryViewController];
                                    navigationController.viewControllers = controllers;
                                    (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
                                    break;
            
                    case 1:
                        {
                            
                            UserAddressViewController *usraddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserAddressViewController"];
            
                            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                            NSArray *controllers = @[usraddressViewController];
                            navigationController.viewControllers = controllers;
                            (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
           
                                break;
                    case 2:
                        {
                            MyCartViewController *cartviewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"MyCartViewController"];
                            cartviewcontroller.navigationtype=@"cart";
                            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                            NSArray *controllers = @[cartviewcontroller];
                            navigationController.viewControllers = controllers;
                            (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
                            break;
            
                    case 3:
                        {
            
                        WishListViewController *usraddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WishListViewController"];
            
                            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                            NSArray *controllers = @[usraddressViewController];
                            navigationController.viewControllers = controllers;
                            (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
            
                        break;
            
                    case 5:
                        {
                        YourProfileViewController *usraddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"YourProfileViewController"];
                        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                        NSArray *controllers = @[usraddressViewController];
                        navigationController.viewControllers = controllers;
                            (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
                            break;
                   case 8:
                    {
                        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                        [login logOut];
                        
#pragma google+logout
                        [[GPPSignIn sharedInstance] signOut];
                        
#pragma normal login details empty
                        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"useremail"];
                        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"pwd"];
                        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"LoginType"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        ViewController *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            
                        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                        NSArray *controllers = @[loginViewController];
                        navigationController.viewControllers = controllers;
                        (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                    }
                            break;
                   case 7:
                    {
                        CarmaStoreViewController *carmastoreViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CarmaStoreViewController"];
                        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                        NSArray *controllers = @[carmastoreViewController];
                        navigationController.viewControllers = controllers;
                        (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                    }
                    default:
                    break;
    }
    





//    switch (_index) {
//        case 0:
//        {
//
//            HomeViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//           
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:homeViewController];
//            navigationController.viewControllers = controllers;
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//        }
//            break;
//        case 1:
//        {
//            AboutUSViewController *aboutUsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsController"];
//            
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:aboutUsViewController];
//            navigationController.viewControllers = controllers;
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//        }
//            break;
//        case 2:
//        {
//            
//            app.lastSelectedIndex=0;
//            ProductListViewController *productViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductListController"];
//            
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:productViewController];
//            navigationController.viewControllers = controllers;
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//        }
//            break;
//        case 3:
//        {
//            SpecialOfferViewController *productViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SpecialOfferController"];
//            
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:productViewController];
//            navigationController.viewControllers = controllers;
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//
//            
//        }
//            break;
//        case 4:
//        {
//            
//            ContactUsViewController *productViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsController"];
//            
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:productViewController];
//            navigationController.viewControllers = controllers;
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//            
//            
//        }
//            break;
//            
//            
//        case 5:
//        {
//            
//             app.wishlistLastIndex=0;
//            WishListViewController *wishListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WishListController"];
//            
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:wishListViewController];
//            navigationController.viewControllers = controllers;
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//            
//            
//            
//            
//        }
//            break;
//            
//        
//        case 6:
//        {
//            
//            
//            app.cartLastSelectedIndex=0;
//            CartDetailsViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"cartDetailsController"];
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:loginView];
//            navigationController.viewControllers = controllers;
//            // [_searchProducts resignFirstResponder];
//            
//         
//            
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//            
//            
//            
//            
//            
//            
//        }
//            break;
//        case 7:
//        {
//            
//                     
//            
//            OrderHistoryViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"orderListController"];
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:loginView];
//            navigationController.viewControllers = controllers;
//            // [_searchProducts resignFirstResponder];
//            
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//            
//            
//            
//            
//            
//            
//        }
//            break;
//            
//        case 8:
//        {
//            
//            
//            LoginViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"mainview"];
//            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//            NSArray *controllers = [NSArray arrayWithObject:loginView];
//            navigationController.viewControllers = controllers;
//        
//            
//            self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
//            
//            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//        
//          
//            
//        }
//            break;
//            
//        default:
//            break;
//    }
//

}

@end
