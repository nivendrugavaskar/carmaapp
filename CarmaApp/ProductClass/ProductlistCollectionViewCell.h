//
//  ProductlistCollectionViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 12/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductListViewprotocol

-(void)buttonadtocart:(id)sender;

@end

@interface ProductlistCollectionViewCell : UICollectionViewCell

@property (assign) id <ProductListViewprotocol> delegate1;
@property (strong, nonatomic) IBOutlet UILabel *lblproductname;
@property (strong, nonatomic) IBOutlet UILabel *lblprice;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewproduct;

- (IBAction)didTapAddtoCart:(id)sender;

@end
