//
//  ProductCollectionViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 12/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductGridViewprotocol

-(void)buttonadtocart:(id)sender;

@end

@interface ProductCollectionViewCell : UICollectionViewCell

@property (assign) id <ProductGridViewprotocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblproductname;
@property (strong, nonatomic) IBOutlet UILabel *lblprice;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewProduct;


- (IBAction)didTapAddtoCart:(id)sender;

@end
