//
//  ProductViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 12/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ProductViewController.h"
#import "AppDelegate.h"
#import "ProductCollectionViewCell.h"
#import "ProductlistCollectionViewCell.h"
#import "DemoTableController.h"
#import "FPPopoverController.h"
#import "ProductDescriptionViewController.h"

#import "MyCartViewController.h"


#import "FBSliderMenuViewController.h"


static NSString * const ListCellIdentifier = @"ListCell";
static NSString * const CellIdentifier = @"Cell";


@interface ProductViewController ()<ProductGridViewprotocol,ProductListViewprotocol,connectionprotocol>
{
    AppDelegate *app;
    NSMutableArray *productArray;
    NSString *selectedview,*selectedservice;
    int selectedtagbutton,pagenumber,totalproduct,productquantity;
    NSString *selectedsort,*sortnumber,*selectedcategoryid;
    BOOL loadpage;
    Connection *conn;
    
    UILabel *badgeno;
}

@end

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setupbarbutton];
#pragma maintain grid and listing
    selectedview=@"List";
    productArray =[[NSMutableArray alloc] init];
#pragma set category selected
    _lblcategory.text=self.selectedcatg;
    loadpage=FALSE;
    selectedservice=@"selectedcategory";
    pagenumber=1;
    totalproduct=0;
    sortnumber=@"0";
    
#pragma set delegate
    self.productdelegate =(FBSliderMenuViewController *)self.menuContainerViewController.leftMenuViewController;
    
}
-(void)setupbarbutton
{
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
    
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setcartbutton];
#pragma badge circle
    UIImageView *badge=[[UIImageView alloc] initWithFrame:btn2.bounds];
    badge.frame=CGRectMake(16, -3, 20, 20);
    badge.contentMode=UIViewContentModeScaleAspectFit;
    badge.image=[UIImage imageNamed:@"circle_icon.png"];
#pragma number label
    badgeno=[[UILabel alloc] initWithFrame:badge.bounds];
    badgeno.frame=CGRectMake(2, 2, 16, 16);
    badgeno.font=[UIFont fontWithName:@"Philosopher" size:12.0f];
    badgeno.textAlignment=NSTextAlignmentCenter;
    badgeno.text=app->totalproduct;
    badgeno.textColor=[UIColor whiteColor];
    [badge addSubview:badgeno];
    // end
    [btn2 addSubview:badge];
    [btn2 addTarget:self action:@selector(cartbutton) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:self.selectedcatg]];
    

    
#pragma productlistfetch
    if(!loadpage)
    {
        NSLog(@"%@",self.selectedcategorydict);
        selectedcategoryid=[self.selectedcategorydict valueForKey:@"id_category"];
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
         NSDictionary *params=@{@"id_category": [self.selectedcategorydict valueForKey:@"id_category"],@"pagenum": [NSString stringWithFormat:@"%d",pagenumber],@"sorttype": sortnumber,@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
        [conn servicecall:params :@"productlistv2.php"];
        //id_category,pagenum
        
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)cartbutton
{
    
    [self.productdelegate buttonclick];
    
   // [self.menuContainerViewController selectcart];
    
   /* MyCartViewController *cartviewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"MyCartViewController"];
    cartviewcontroller.navigationtype=@"cart";
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:cartviewcontroller];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];*/
    
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma response delegate
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"selectedcategory"])
    {
    if(!connectiondict)
    {
        return;
    }
    else
    {
        NSMutableArray *localarray=[[connectiondict valueForKeyPath:@"result"] mutableCopy];
        totalproduct=(int)[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"totalproduct"]].integerValue;
        productquantity=(int)[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"quantity"]].integerValue;
        if(localarray.count>0)
        {
            for(int i=0;i<localarray.count;i++)
            {
                [productArray addObject:[localarray[i] mutableCopy]];
            }
        }
        
#pragma showing no product
        if(productArray.count==0)
        {
            _lblnoproduct.hidden=NO;
        }
        else
        {
           _lblnoproduct.hidden=YES;
        }
        
        [self.CollectionView reloadData];
        
    }
    }
    else if([selectedservice isEqualToString:@"categoryfilter"]||[selectedservice isEqualToString:@"sortfilter"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
            NSMutableArray *localarray=[[connectiondict valueForKeyPath:@"result"] mutableCopy];
            totalproduct=(int)[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"totalproduct"]].integerValue;
            productquantity=(int)[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"quantity"]].integerValue;
            if(localarray.count>0)
            {
                for(int i=0;i<localarray.count;i++)
                {
                    [productArray addObject:[localarray[i] mutableCopy]];
                }
            }
            
#pragma showing no product
            if(productArray.count==0)
            {
                _lblnoproduct.hidden=NO;
            }
            else
            {
                _lblnoproduct.hidden=YES;
            }
            [self.CollectionView reloadData];
            
        }
    }
    else if([selectedservice isEqualToString:@"addcart"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
            
            UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:[connectiondict valueForKey:@"msg"] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alrt show];
#pragma new code
            app->totalproduct=[[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"total_product_in_cart"]] mutableCopy];
            [[NSUserDefaults standardUserDefaults] setValue:app->totalproduct forKey:@"cartbadge"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            badgeno.text=app->totalproduct;
        }
    }
    
}

#pragma delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if(scrollView.contentOffset.y+scrollView.frame.size.height==scrollView.contentSize.height)
    {
        
        NSLog(@"call service here");
        pagenumber++;
        if(((pagenumber-1)*10)<=totalproduct)
        {
            selectedservice=@"selectedcategory";
           NSDictionary *params=@{@"id_category": [self.selectedcategorydict valueForKey:@"id_category"],@"pagenum": [NSString stringWithFormat:@"%d",pagenumber],@"sorttype": sortnumber,@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
            [conn servicecall:params :@"productlistv2.php"];
           // [self eventlistservice];
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"pushtoproductdescription" sender:indexPath];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return productArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell;
    if([selectedview isEqualToString:@"List"])
    {
        ProductlistCollectionViewCell *cell=(ProductlistCollectionViewCell *)Cell;
        
        cell =(ProductlistCollectionViewCell *)[self.CollectionView dequeueReusableCellWithReuseIdentifier:ListCellIdentifier forIndexPath:indexPath];
        cell.delegate1=self;
        cell.lblproductname.text=[NSString stringWithFormat:@"%@",[productArray[indexPath.row] valueForKey:@"name"]];
        
        float price=[NSString stringWithFormat:@"%@",[productArray[indexPath.row] valueForKey:@"price"]].floatValue;
        
        cell.lblprice.text=[NSString stringWithFormat:@"$%.2f",price];
        
        //new code for image load
        
//        NSArray *imagearray=[productArray[indexPath.row] valueForKey:@"images"];
//        NSString *imagepath=[NSString stringWithFormat:@"%@",imagearray[0]];
//       // NSString *imageURL = [NSString stringWithFormat: @"www.xyz.image.png"];
//        AsyncImageView *async = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.imgViewproduct.frame.size.width, cell.imgViewproduct.frame.size.height)];
//        async.backgroundColor=[UIColor clearColor];
//        [async loadImageFromURL:[NSURL URLWithString:imagepath]];
//        if([cell.imgViewproduct.subviews count]==0)
//        {
//        [cell.imgViewproduct addSubview:async];
//        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *imagearray=[productArray[indexPath.row] valueForKey:@"images"];
            NSString *imagepath=[NSString stringWithFormat:@"%@",imagearray[0]];
            //imagepath=[imagepath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            
            if(activityIndicator)
            {
                [activityIndicator stopAnimating];
                [activityIndicator removeFromSuperview];
            }
            
            activityIndicator.color=[UIColor brownColor];
            activityIndicator.center = CGPointMake(CGRectGetMidX(cell.imgViewproduct.bounds), CGRectGetMidY(cell.imgViewproduct.bounds));
            activityIndicator.hidesWhenStopped = YES;
            
            __block BOOL successfetchimage=FALSE;
            __weak UIImageView *setimage = cell.imgViewproduct;
            
            
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
            [cell.imgViewproduct setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placeholderview.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 //setimage.contentMode=UIViewContentModeScaleToFill;
                 setimage.image=image;
                 successfetchimage=TRUE;
                 [activityIndicator stopAnimating];
                 [activityIndicator removeFromSuperview];
                 
             }
             
                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 [activityIndicator stopAnimating];
                 [activityIndicator removeFromSuperview];
             }];
            
            
            if(!successfetchimage)
            {
                if(![imagepath isEqualToString:@""]||imagepath)
                {
                    [cell.imgViewproduct addSubview:activityIndicator];
                    [activityIndicator startAnimating];
                }
                else
                {
                    [activityIndicator stopAnimating];
                    [activityIndicator removeFromSuperview];
                }
            }
            
            [cell setNeedsLayout];
            
        });
        
       
        
        Cell=cell;
        
    }
    else
    {
     ProductCollectionViewCell *cell=(ProductCollectionViewCell *)Cell;
     cell =(ProductCollectionViewCell *)[self.CollectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
     cell.delegate=self;
        
        cell.lblproductname.text=[NSString stringWithFormat:@"%@",[productArray[indexPath.row] valueForKey:@"name"]];
        
        float price=[NSString stringWithFormat:@"%@",[productArray[indexPath.row] valueForKey:@"price"]].floatValue;
        
        cell.lblprice.text=[NSString stringWithFormat:@"$%.2f",price];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Refresh image view here
            //new code for image load
            NSArray *imagearray=[productArray[indexPath.row] valueForKey:@"images"];
            NSString *imagepath=[NSString stringWithFormat:@"%@",imagearray[0]];
            //imagepath=[imagepath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            activityIndicator.color=[UIColor brownColor];
            activityIndicator.center = CGPointMake(CGRectGetMidX(cell.imgViewProduct.bounds), CGRectGetMidY(cell.imgViewProduct.bounds));
            activityIndicator.hidesWhenStopped = YES;
            
            __block BOOL successfetchimage=FALSE;
            __weak UIImageView *setimage = cell.imgViewProduct;
            
            
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
            [cell.imgViewProduct setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placeholderview.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 //setimage.contentMode=UIViewContentModeScaleToFill;
                 setimage.image=image;
                 successfetchimage=TRUE;
                 [activityIndicator stopAnimating];
                 [activityIndicator removeFromSuperview];
             }
             
                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 [activityIndicator stopAnimating];
                 [activityIndicator removeFromSuperview];
             }];
            
            
            if(!successfetchimage)
            {
                if(![imagepath isEqualToString:@""]||imagepath)
                {
                    [cell.imgViewProduct addSubview:activityIndicator];
                    [activityIndicator startAnimating];
                }
                else
                {
                    [activityIndicator stopAnimating];
                    [activityIndicator removeFromSuperview];
                }
            }
            [cell setNeedsLayout];
        });
        
     Cell=cell;
    }
    return Cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float width;
    if([selectedview isEqualToString:@"List"])
    {
        width=self.CollectionView.frame.size.width;
        return CGSizeMake(width, 130);
    }
    else
    {
        width=(self.CollectionView.frame.size.width/2)-10;
        return CGSizeMake(width, 220);
    }
    
}
- (IBAction)didTapViewMode:(id)sender
{
    if([selectedview isEqualToString:@"List"])
    {
        selectedview=@"Grid";
        _selectedviewmodeimg.image=[UIImage imageNamed:@"gridviewicon.png"];
    }
    else
    {
        selectedview=@"List";
        _selectedviewmodeimg.image=[UIImage imageNamed:@"gridicon.png"];
    }
    [self.CollectionView reloadData];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoproductdescription"])
    {
        NSIndexPath *indexpath=(NSIndexPath *)sender;
        ProductDescriptionViewController *inst=(ProductDescriptionViewController *)segue.destinationViewController;
        inst.selectedproductdict=productArray[indexpath.row];
        
    }
}


- (IBAction)didTapCategory:(id)sender
{
    selectedsort=@"sortcategory";
    [self popover:sender];
}

-(IBAction)popover:(id)sender
{
    //NSLog(@"popover retain count: %d",[popover retainCount]);
    UIButton *btn=(UIButton *)sender;
    selectedtagbutton=(int)btn.tag;
    
    SAFE_ARC_RELEASE(popover); popover=nil;
    
    //the controller we want to present as a popover
    DemoTableController *controller = [[DemoTableController alloc] initWithStyle:UITableViewStylePlain];
    controller.delegate = self;
    if(btn.tag==5)
    {
    controller.headtitle=@"Category";
    controller.categoryarray=[[NSMutableArray alloc] init];
        
        for(int i=0;i<(self.categoryArray).count;i++)
        {
            [controller.categoryarray addObject:[(self.categoryArray)[i] valueForKey:@"name"]];
        }
        
   // controller.categoryarray=[[NSMutableArray alloc] initWithObjects:@"Category1",@"Category2",@"Category3",@"Category4",@"Category5", nil];
    }
    else
    {
    controller.headtitle=@"Sort By";
    controller.categoryarray=[[NSMutableArray alloc] initWithObjects:@"Price: Highest to Lowest",@"Price: Lowest to Highest",@"In Stock", nil];
    }
    
    popover = [[FPPopoverKeyboardResponsiveController alloc] initWithViewController:controller];
    popover.tint = FPPopoverDefaultTint;
    popover.keyboardHeight = _keyboardHeight;
    
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        popover.contentSize = CGSizeMake(300, 500);
//    }
//    else {
//        popover.contentSize = CGSizeMake(200, 300);
//    }
    
    if([selectedsort isEqualToString:@"sortcategory"])
    {
        popover.contentSize = CGSizeMake(250, 300);
       
    }
    else
    {
        popover.contentSize = CGSizeMake(200, 175);
    }
    
    
        //sender is the UIButton view
        popover.arrowDirection = FPPopoverArrowDirectionUp;
        [popover presentPopoverFromView:sender];
    
}

-(void)selectedTableRow:(NSUInteger)rowNum
{
   // NSLog(@"SELECTED ROW %lu",(unsigned long)rowNum);
    [popover dismissPopoverAnimated:YES];
    
    if(selectedtagbutton==5)
    {
    selectedservice=@"categoryfilter";
    productArray=[[NSMutableArray alloc] init];
    pagenumber=1;
    selectedcategoryid=[NSString stringWithFormat:@"%@",[(self.categoryArray)[rowNum] valueForKey:@"id_category"]];
        
    NSDictionary *params=@{@"id_category": selectedcategoryid,@"pagenum": [NSString stringWithFormat:@"%d",pagenumber],@"sorttype": sortnumber,@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
    [conn servicecall:params :@"productlistv2.php"];
    }
    else
    {
        sortnumber=[NSString stringWithFormat:@"%lu",(unsigned long)rowNum+1];
        selectedservice=@"sortfilter";
        productArray=[[NSMutableArray alloc] init];
        pagenumber=1;
        NSDictionary *params=@{@"id_category": selectedcategoryid,@"pagenum": [NSString stringWithFormat:@"%d",pagenumber],@"sorttype": sortnumber,@"id_customer": [NSString stringWithFormat:@"%@",[app->logindetails valueForKeyPath:@"result.id_customer"]]};
        [conn servicecall:params :@"productlistv2.php"];
    }
    
    
}
-(void)selectedTableRowData:(NSString *)selectedcategory
{
    // NSLog(@"Selected Category %@",selectedcategory);
    if(selectedtagbutton==5)
    {
    (self.lblcategory).text = selectedcategory;
#pragma set navigation title
        if([self.navigationController.navigationBar viewWithTag:30])
        {
            [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
        }
        self.selectedcatg=selectedcategory;
        [self.navigationController.navigationBar addSubview:[customlbl setlabel:self.selectedcatg]];
    }
    else
    {
        (self.lblsortby).text = selectedcategory;
    }
    [popover dismissPopoverAnimated:YES];
}
- (IBAction)didTapSort:(id)sender {
    
    selectedsort=@"sortby";
    [self popover:sender];
}


-(void)buttonadtocart:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.CollectionView];
     NSIndexPath *indexPath = [self.CollectionView indexPathForItemAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    selectedservice=@"addcart";
    NSLog(@"%@",app->logindetails);
    NSDictionary *param=@{@"id_customer": [app->logindetails valueForKeyPath:@"result.id_customer"],@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"],@"id_product": [productArray[indexPath.row] valueForKeyPath:@"id_product"],@"quantity": @"1"};
    [conn servicecall:param :@"addtocartLoginuser.php"];
    
}
@end
