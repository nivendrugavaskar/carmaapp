//
//  ProductViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 12/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FPPopoverController.h"
#import "ARCMacros.h"
#import "FPPopoverKeyboardResponsiveController.h"

@protocol productprotocol
- (void)buttonclick;


@end

@interface ProductViewController : UIViewController<FPPopoverControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    FPPopoverKeyboardResponsiveController *popover;
    CGFloat _keyboardHeight;
}
-(void)selectedTableRow:(NSUInteger)rowNum;
-(void)selectedTableRowData:(NSString *)selectedcategory;

@property (strong, nonatomic) IBOutlet UICollectionView *CollectionView;
@property (strong, nonatomic) IBOutlet UIImageView *selectedviewmodeimg;
@property (strong, nonatomic) IBOutlet UIButton *btnCategory;
@property (strong, nonatomic) IBOutlet UIButton *btnsort;
@property (strong, nonatomic) IBOutlet UILabel *lblcategory;
@property (strong, nonatomic) IBOutlet UILabel *lblsortby;
@property (strong, nonatomic) IBOutlet UILabel *lblnoproduct;

@property NSMutableArray *categoryArray;
@property NSMutableDictionary *selectedcategorydict;
@property NSString *selectedcatg;


@property (assign) id <productprotocol> productdelegate;

@end
