//
//  MyCartTableViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 11/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCartTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgproduct;
@property (strong, nonatomic) IBOutlet UILabel *lblprice;
@property (strong, nonatomic) IBOutlet UILabel *lblname;
@property (strong, nonatomic) IBOutlet UILabel *lblqty;

@end
