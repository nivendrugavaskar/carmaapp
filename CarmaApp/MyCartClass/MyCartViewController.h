//
//  MyCartViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 11/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCartViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UILabel *no_cart_found;
@property (strong, nonatomic) IBOutlet UILabel *lblsubtotal;
@property (strong, nonatomic) IBOutlet UIView *subtotalView;
- (IBAction)didtapCheckout:(id)sender;

@property NSString *navigationtype;
@end
