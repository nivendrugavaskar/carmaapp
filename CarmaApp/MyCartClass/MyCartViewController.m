//
//  MyCartViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 11/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "MyCartViewController.h"
#import "AppDelegate.h"
#import "MyCartTableViewCell.h"

@interface MyCartViewController ()<connectionprotocol,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
     NSString *selectedservice;
    AppDelegate *app;
    Connection *conn;
    NSMutableArray *cartArray;
    BOOL loadlist;
    
    NSIndexPath *deletedindex;
}

@end

@implementation MyCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setbarbuttons];
    loadlist=FALSE;
}

-(void)setbarbuttons
{
    self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;
    
#pragma left menu bar
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    self.navigationItem.leftBarButtonItems = @[leftbarbutton];
    
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"My Cart"]];
    
    selectedservice=@"Fetchcart";
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    NSDictionary *param=@{@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"]};
    [conn servicecall:param :@"cartdetails.php"];
}
#pragma handle response
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"Fetchcart"])
    {
        
        app->totalproduct=[[NSString stringWithFormat:@"%@",[connectiondict valueForKeyPath:@"total_product_in_cart"]] mutableCopy];
        [[NSUserDefaults standardUserDefaults] setValue:app->totalproduct forKey:@"cartbadge"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        cartArray=[[connectiondict valueForKey:@"result"] mutableCopy];
        _lblsubtotal.text=[NSString stringWithFormat:@"Subtotal-$%.2f",[NSString stringWithFormat:@"%@",[connectiondict valueForKey:@"subtotal"]].floatValue];
    }
    else if([selectedservice isEqualToString:@"deletecart"])
    {
        [cartArray removeObjectAtIndex:deletedindex.row];
        [self.tblView deleteRowsAtIndexPaths:@[deletedindex] withRowAnimation:UITableViewRowAnimationLeft];
        
        selectedservice=@"Fetchcart";
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        NSDictionary *param=@{@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"]};
        [conn servicecall:param :@"cartdetails.php"];
        
    }
    if(cartArray.count>0)
    {
        _no_cart_found.hidden=YES;
        _subtotalView.hidden=NO;
    }
    else
    {
        _no_cart_found.hidden=NO;
        _subtotalView.hidden=YES;
    }
    [self.tblView reloadData];
    
    
   
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return cartArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"Cell";
    
    MyCartTableViewCell *cell = (MyCartTableViewCell *)[self.tblView dequeueReusableCellWithIdentifier:cellNameFirst];
    
    
    if (cell == NULL)
    {
        cell = [[MyCartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNameFirst];
    }
    
    cell.lblname.text=[NSString stringWithFormat:@"%@",[cartArray[indexPath.row] valueForKey:@"name"]];
    cell.lblprice.text=[NSString stringWithFormat:@"$%.2f",[[cartArray[indexPath.row] valueForKey:@"total"] floatValue]];
    cell.lblqty.text=[NSString stringWithFormat:@"%@",[cartArray[indexPath.row] valueForKey:@"quantity"]];
    
    //new code for image load
    NSString *imagepath=[NSString stringWithFormat:@"%@",[cartArray[indexPath.row] valueForKey:@"imagelink"]];
    //imagepath=[imagepath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor brownColor];
    activityIndicator.center = CGPointMake(CGRectGetMidX(cell.imgproduct.bounds), CGRectGetMidY(cell.imgproduct.bounds));
    activityIndicator.hidesWhenStopped = YES;
    
    __block BOOL successfetchimage=FALSE;
    __weak UIImageView *setimage = cell.imgproduct;
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
    [cell.imgproduct setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placeholderview.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         setimage.contentMode=UIViewContentModeScaleToFill;
         setimage.image=image;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
     }
     
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator removeFromSuperview];
     }];
    
    
    if(!successfetchimage)
    {
        if(![imagepath isEqualToString:@""]||imagepath)
        {
            [cell.imgproduct addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)didTapDelete:(id)sender {
    
   
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
     deletedindex=indexPath;
    UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Do you want to delete product from cart?" message:[cartArray[indexPath.row] valueForKeyPath:@"name"] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alrt.tag=100;
    [alrt show];
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
           deletedindex=[[NSIndexPath alloc] init];
        }
        else
        {
            selectedservice=@"deletecart";
            NSString *productid=[NSString stringWithFormat:@"%@",[cartArray[deletedindex.row] valueForKeyPath:@"id_product"]];
            NSDictionary *params=@{@"id_cart": [app->logindetails valueForKeyPath:@"result.id_cart"],@"id_product": productid};
            [conn servicecall:params :@"cartdelete.php"];
        }
    }
}


- (IBAction)didtapCheckout:(id)sender {
    [self performSegueWithIdentifier:@"pushtocheckout" sender:self];
}
@end
