//
//  ShippingViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 27/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingViewController : UIViewController
- (IBAction)didTapexpress:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *expressimgview;
- (IBAction)didTapstandard:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *standardimageview;
- (IBAction)didTapagree:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *chkimageview;
@property (strong, nonatomic) IBOutlet UILabel *lbloptionfirst;
@property (strong, nonatomic) IBOutlet UILabel *lblpriceoptionfirst;
@property (strong, nonatomic) IBOutlet UILabel *lbloptionsecond;
@property (strong, nonatomic) IBOutlet UILabel *lblpriceoptionsecond;

@end
