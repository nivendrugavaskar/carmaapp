//
//  ShippingViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 27/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ShippingViewController.h"
#import "AppDelegate.h"

@interface ShippingViewController ()<connectionprotocol>
{
    BOOL ischecked;
    NSString *selecteddelevery,*selectedservice;
    BOOL loadpage;
    AppDelegate *app;
    Connection *conn;
    
    NSMutableArray *shippedoptionArray;
    
}

@end

@implementation ShippingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma left menu bar
    UIButton *btn=[Custombarbutton setbarbutton];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn];
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton1=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =5;
    self.navigationItem.leftBarButtonItems = @[leftbarbutton1,fixedSpaceBarButtonItem,leftbarbutton];
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setaddtocartbutton:@"Continue"];
    [btn2 addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
#pragma initial setup
    ischecked=FALSE;
    selecteddelevery=@"Express";
    loadpage=FALSE;
    
    NSLog(@"fetched dict=%@",[[SingleToneClass sharedManager] shippinginfoInfoDict]);
    
#pragma service call
    selectedservice=@"FetchShip";
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    NSDictionary *param=@{@"postcode": [[[SingleToneClass sharedManager] shippinginfoInfoDict] valueForKey:@"postcode"],@"id_country": [[[SingleToneClass sharedManager] shippinginfoInfoDict] valueForKey:@"id_country"]};
    
    [conn servicecall:param :@"shippingmethod.php"];
    
    
}
#pragma handle response
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"FetchShip"])
    {
        shippedoptionArray=[[NSMutableArray alloc] init];
        shippedoptionArray=[connectiondict valueForKey:[@"result" mutableCopy]];
        [self loaddata];
    }
    
}

#pragma restruct UI
-(void)loaddata
{
    if(shippedoptionArray.count==2)
    {
    _lbloptionfirst.text=[NSString stringWithFormat:@"%@",[shippedoptionArray[0] valueForKey:@"name"]];
    float price=[NSString stringWithFormat:@"%@",[shippedoptionArray[0] valueForKey:@"price"]].floatValue;
        if(price==0)
        {
          _lblpriceoptionfirst.text=@"Free";
        }
        else
        {
       _lblpriceoptionfirst.text=[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%.2f",price]];
        }
    
    _lbloptionsecond.text=[NSString stringWithFormat:@"%@",[shippedoptionArray[1] valueForKey:@"name"]];
        
        float price1=[NSString stringWithFormat:@"%@",[shippedoptionArray[1] valueForKey:@"price"]].floatValue;
        if(price1==0)
        {
            _lblpriceoptionsecond.text=@"Free";
        }
        else
        {
            _lblpriceoptionsecond.text=[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%.2f",price1]];
        }
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Shipping"]];
    
    if(!loadpage)
    {
        
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

-(void)next
{
   if(!ischecked)
   {
       UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Required" message:@"You must have to agree terms of service" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
       [alert show];
   }
    else
    {
        
    }
    
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapexpress:(id)sender
{
    if([selecteddelevery isEqualToString:@"Standard"])
    {
        selecteddelevery=@"Express";
        _expressimgview.image=[UIImage imageNamed:@"radio_btn.png"];
        _standardimageview.image=[UIImage imageNamed:@"radio_btn1.png"];
    }
}
- (IBAction)didTapstandard:(id)sender
{
    if([selecteddelevery isEqualToString:@"Express"])
    {
        selecteddelevery=@"Standard";
        _expressimgview.image=[UIImage imageNamed:@"radio_btn1.png"];
        _standardimageview.image=[UIImage imageNamed:@"radio_btn.png"];
    }
}
- (IBAction)didTapagree:(id)sender
{
    if(!ischecked)
    {
        _chkimageview.image=[UIImage imageNamed:@"checkbox.png"];
        ischecked=TRUE;
        
    }
    else
    {
        _chkimageview.image=[UIImage imageNamed:@"checkbox2.png"];
        ischecked=FALSE;
        
    }
}
@end
