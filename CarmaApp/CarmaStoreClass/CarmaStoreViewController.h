//
//  CarmaStoreViewController.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 09/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol storeprotocol
- (void)buttonclick;

@end

@interface CarmaStoreViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UITextField *txtlocarion;
@property (strong, nonatomic) IBOutlet UITextField *txtradius;
@property (strong, nonatomic) IBOutlet UIToolbar *keyBoardToolBar;
@property (strong, nonatomic) IBOutlet UIView *cursorview;
@property (strong, nonatomic) IBOutlet UIView *locationview;


@property UITextField *activetextfield;

@property (assign) id <storeprotocol> storedelegate;

@end
