//
//  CarmaStoreViewController.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 09/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "CarmaStoreViewController.h"
#import "AppDelegate.h"
#import "CustomAnnotation.h"
#import "CarmaStoreTableViewCell.h"

#import "FBSliderMenuViewController.h"

#define METERS_PER_MILE 7000

@interface CarmaStoreViewController ()<connectionprotocol,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{

    AppDelegate *app;
    int tagger;
    NSMutableArray *storearray,*timeArray,*daysarray,*radiusArray;
    Connection *conn;
    NSArray *json;
    CGPoint pointtrack,pointtrackcontentoffset;
    NSString *selectedservice,*selectedRadius;
    UIPickerView *radiuspicker;
    int radiusindex;
}

@end

@implementation CarmaStoreViewController

@synthesize keyBoardToolBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
#pragma set delegate
    self.storedelegate =(FBSliderMenuViewController *)self.menuContainerViewController.leftMenuViewController;
    
    [self setbarbuttons];
    tagger=0;
#pragma set fame to manage animation
    pointtrackcontentoffset=self.view.frame.origin;
    selectedservice=@"Fetchstoreall";
    storearray=[[NSMutableArray alloc] init];
    radiusArray=[[NSMutableArray alloc] initWithObjects:@"15",@"25",@"50",@"100", nil];
    radiusindex=0;
    selectedRadius=radiusArray[radiusindex];
    _txtradius.text=selectedRadius;
    daysarray=[[NSMutableArray alloc] initWithObjects:@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday", nil];
    
    
#pragma set toolbar
    for (id v in self.view.subviews) {
        if([v isKindOfClass:[UIView class]])
        {
            UIView *innerview=(UIView *)v;
            for (id v in innerview.subviews)
            {
                if([v isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfld=(UITextField *)v;
                    txtfld.inputAccessoryView=keyBoardToolBar;
                }
            }
        }
    }
    
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    [conn servicecall:nil :@"allstore.php"];

}

- (NSMutableArray *)parseJSONCities{
    
    NSMutableArray *retval = [[NSMutableArray alloc]init];
//    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"capitals"
//                                                         ofType:@"json"];
//    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
//    NSError *error = nil;
//    NSArrjson = [NSJSONSerialization JSONObjectWithData:data
//                                                    options:kNilOptions
//                                                      error:&error];
    
    for (CustomAnnotation *record in json)
    {
        
        CustomAnnotation *temp = [[CustomAnnotation alloc] init];
        temp.title = [record valueForKey:@"name"];
        temp.subtitle = [record valueForKey:@"address1"];
        temp.coordinate = CLLocationCoordinate2DMake([[record valueForKey:@"latitude"]floatValue], [[record valueForKey:@"longitude"]floatValue]);
        
        
        
        temp.tag=tagger++;
        [retval addObject:temp];
        [storearray addObject:record];
        
    }
    
   
    return retval;
}

#pragma handle response
-(void)sucessdone:(NSDictionary *)connectiondict
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if([selectedservice isEqualToString:@"Fetchstoreall"])
    {
    if(!connectiondict)
    {
        return;
    }
    else
    {
#pragma new code
        __block NSArray *annoations;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
//            annoations = [self stores:connectiondict];
            
            json=[connectiondict valueForKey:@"result"];
            
            annoations = [self parseJSONCities];
            
           // CustomAnnotation *ann=[annoations objectAtIndex:0];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                
               // [self.mapView setCenterCoordinate:ann.coordinate animated:YES];
                
                [self.mapView addAnnotations:annoations];
                
                CustomAnnotation *ann=annoations[(int)(json.count)/2];
                [_mapView selectAnnotation:ann animated:YES];
                CLLocationCoordinate2D zoomLocation;
                zoomLocation=ann.coordinate;
                [self.mapView setCenterCoordinate:zoomLocation animated:YES];
                MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 8*METERS_PER_MILE, 8*METERS_PER_MILE);
                [_mapView setRegion:viewRegion animated:YES];
                
            });
        });
    }
    }
    else if([selectedservice isEqualToString:@"searchbased"])
    {
        if(!connectiondict)
        {
            return;
        }
        else
        {
#pragma new code
            __block NSArray *annoations;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                //            annoations = [self stores:connectiondict];
                
                json=[connectiondict valueForKey:@"result"];
                
                annoations = [self parseJSONCities];
                
                // CustomAnnotation *ann=[annoations objectAtIndex:0];
                
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    
                    // [self.mapView setCenterCoordinate:ann.coordinate animated:YES];
                    _txtlocarion.text=@"";
                    selectedRadius=@"15";
                    radiusindex=0;
                    [self.mapView addAnnotations:annoations];
                    
                    CustomAnnotation *ann=annoations[(int)(json.count)/2];
                    [_mapView selectAnnotation:ann animated:YES];
                    CLLocationCoordinate2D zoomLocation;
                    zoomLocation=ann.coordinate;
                    [self.mapView setCenterCoordinate:zoomLocation animated:YES];
                    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 8*METERS_PER_MILE, 8*METERS_PER_MILE);
                    [_mapView setRegion:viewRegion animated:YES];
                    
                });
            });
        }
    }
    
}
#pragma set barbuttons
-(void)setbarbuttons
{
    self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;
#pragma left menu bar
    UIButton *btn1=[Custombarbutton setmenubarbutton];
    [btn1 addTarget:self action:@selector(slidemenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn1];
    self.navigationItem.leftBarButtonItems = @[leftbarbutton];
    
#pragma right menu bar
    UIButton *btn2=[Custombarbutton setcartbutton];
#pragma badge circle
    UIImageView *badge=[[UIImageView alloc] initWithFrame:btn2.bounds];
    badge.frame=CGRectMake(16, -3, 20, 20);
    badge.contentMode=UIViewContentModeScaleAspectFit;
    badge.image=[UIImage imageNamed:@"circle_icon.png"];
#pragma number label
    UILabel *badgeno=[[UILabel alloc] initWithFrame:badge.bounds];
    badgeno.frame=CGRectMake(2, 2, 16, 16);
    badgeno.font=[UIFont fontWithName:@"Philosopher" size:12.0f];
    badgeno.textAlignment=NSTextAlignmentCenter;
    badgeno.text=app->totalproduct;
    badgeno.textColor=[UIColor whiteColor];
    [badge addSubview:badgeno];
    // end
    [btn2 addSubview:badge];
    [btn2 addTarget:self action:@selector(cartbutton) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightbarbutton=[[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.rightBarButtonItems = @[rightbarbutton];
    
    _locationview.layer.borderColor=[UIColor whiteColor].CGColor;
    _cursorview.layer.borderColor=[UIColor whiteColor].CGColor;
    _locationview.layer.borderWidth=1.0;
    _cursorview.layer.borderWidth=1.0;
    
    
}
-(void)slidemenu
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)cartbutton
{
    [self.storedelegate buttonclick];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.navigationController.navigationBar viewWithTag:30])
    {
        [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:[customlbl setlabel:@"Carma Store"]];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController.navigationBar viewWithTag:30] removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma tableview delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellNameFirst = @"Cell";
    CarmaStoreTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:cellNameFirst forIndexPath:indexPath];
    
    cell.lblday.text=daysarray[indexPath.row];
    cell.lbltime.text=timeArray[indexPath.row];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return timeArray.count;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor colorWithRed:37/255.0f green:93/255.0f blue:19/255.0f alpha:1.0];
    else
        cell.backgroundColor = [UIColor clearColor];
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    MKAnnotationView *pinView = nil;
    if(annotation != self.mapView.userLocation)
    {
        static NSString *defaultPinID = @"com.invasivecode.pin";
        pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        pinView.canShowCallout = YES;
      //  UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
       // pinView.rightCalloutAccessoryView = rightButton;
        pinView.image = [UIImage imageNamed:@"location_icon.png"];//as suggested by Squatch
    }
    else
    {
        return nil;
    }
    return pinView;
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    CustomAnnotation *vw=(CustomAnnotation *)view.annotation;
    NSLog(@"selected annotation%d",vw.tag);
    //NSLog(@"%@",[storearray objectAtIndex:vw.tag]);
    
    timeArray=[[NSMutableArray alloc] initWithArray:[[storearray[vw.tag] valueForKey:@"appointment"] mutableCopy]];
    
    [self.tblView reloadData];
    
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if([view.annotation isKindOfClass:[MKUserLocation class]])
    {
        return;
    }
    else
    {
        CustomAnnotation *vw=(CustomAnnotation *)view.annotation;
        [self.mapView deselectAnnotation:vw animated:YES];
        NSLog(@"%d",vw.tag);
        NSLog(@"%@",storearray[vw.tag]);
    }
    
}
#pragma textfield delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activetextfield=textField;
    pointtrack = textField.superview.frame.origin;
    
    CGRect rect=self.view.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    
    if(self.activetextfield==_txtradius)
    {
        radiuspicker=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 250)];
        radiuspicker.dataSource = self;
        radiuspicker.delegate = self;
        radiuspicker.tag=1001;
        [radiuspicker setShowsSelectionIndicator:YES];
        _txtradius.inputView = radiuspicker;
    }
    
    if(SCREEN_HEIGHT==480||SCREEN_HEIGHT==568)
    {
        if(pointtrack.y>120)
        {
            if(pointtrack.y>350)
            {
                pointtrack.y=340;
            }
            rect.origin.y-=pointtrack.y-130;
        }
    }
    
    else if(SCREEN_HEIGHT==667)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-100;
        }
    }
    else if(SCREEN_HEIGHT==736)
    {
        if(pointtrack.y>150.0)
        {
            rect.origin.y-=pointtrack.y-120;
        };
    }
    else
    {
        
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
    return NO;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if(!(self.activeTextField.tag==1||self.activeTextField.tag==2))
//    {
//        if([string isEqualToString:@" "])
//        {
//            return NO;
//        }
//    }
    return YES;
}

-(void)PickerSelection
{
    
        if(radiusArray.count>0)
        {
           radiusindex = (int)[radiuspicker selectedRowInComponent:0];
            _txtradius.text=radiusArray[radiusindex];
            selectedRadius=[NSString stringWithFormat:@"%@",radiusArray[radiusindex]];
            
        }
        else
        {
            [self dismisskeyboard];
        }
    }


#pragma dismiss keyboard
-(void)dismisskeyboard
{
    [self.activetextfield resignFirstResponder];
    
    if(self.activetextfield==_txtradius)
    {
        [self PickerSelection];
    }
    
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
}

#pragma mark Picker Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@", radiusArray[row]];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return radiusArray.count;
}
- (IBAction)didTapDone:(id)sender {
    [self dismisskeyboard];
}
- (IBAction)didTapPrevious:(id)sender {
    
    NSInteger nextTag = self.activetextfield.tag - 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapNext:(id)sender {
    NSInteger nextTag = self.activetextfield.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
}
- (IBAction)didTapSearch:(id)sender {
    [self dismisskeyboard];
    
    if(![NSString validation:_txtlocarion.text])
    {
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Required" message:@"please fill any location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
        return;
    }
    
    selectedservice=@"searchbased";
    tagger=0;
    [self.mapView removeAnnotations:self.mapView.annotations];
    conn=[[Connection alloc] init];
    conn.connectionDelegate=self;
    
    _txtlocarion.text=[_txtlocarion.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *param=@{@"searchtext": _txtlocarion.text,@"distance": _txtradius.text};
    
    [conn servicecall:param :@"searchstore.php"];
    
}

@end
