//
//  CarmaStoreTableViewCell.h
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 10/09/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarmaStoreTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblday;
@property (strong, nonatomic) IBOutlet UILabel *lbltime;

@end
