//
//  DemoTableControllerViewController.m
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import "DemoTableController.h"
#import "ProductViewController.h"
#import "AppDelegate.h"
@interface DemoTableController ()

@end

@implementation DemoTableController
@synthesize delegate=_delegate;
@synthesize categoryarray;


- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.title = _headtitle;
    


}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return categoryarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Demo";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   // cell.contentView.backgroundColor=[UIColor blueColor];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
     (cell.textLabel).font = [UIFont fontWithName:@"Philosopher" size:14.0f];
     cell.textLabel.textColor=[UIColor brownColor];
     cell.textLabel.text = [NSString stringWithFormat:@"%@",categoryarray[indexPath.row]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   // cell.contentView.backgroundColor=[UIColor blueColor];
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.delegate respondsToSelector:@selector(selectedTableRow:)])
    {
        [self.delegate selectedTableRow:indexPath.row];
        
    }
    if([self.delegate respondsToSelector:@selector(selectedTableRowData:)])
    {
        [self.delegate selectedTableRowData:categoryarray[indexPath.row]];
        
    }
}




@end
