//
//  DemoTableControllerViewController.h
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProductViewController;

@interface DemoTableController : UITableViewController
@property(nonatomic,assign) ProductViewController *delegate;

@property(nonatomic,strong) NSString *headtitle;

@property(nonatomic,strong)NSMutableArray *categoryarray;


@end
